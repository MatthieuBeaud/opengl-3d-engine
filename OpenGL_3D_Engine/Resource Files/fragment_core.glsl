#version 440


in vec3 gs_normal;
in vec3 gs_pos;
in vec4 gs_pos_light_space;

out vec4 fs_color;

//material 
uniform vec4 color = vec4(1.0);
uniform vec4 subsurfaceColor = vec4(1.0);
uniform int shininess = 32;
uniform float density = 1.0;

uniform vec3 ambientLight = vec3(1.0);

uniform vec3 sunLightsDirections[10];	//max 10 sun lights
uniform vec3 sunLightsColors[10];
uniform unsigned int nbOfSunLights = 0;

uniform vec3 pointLightsPositions[6]; //max 6 point lights
uniform vec3 pointLightsColors[6];
uniform samplerCube lightDepthCubemap[6];
uniform samplerCube lightNormalDataCubemap[6];
uniform unsigned int nbOfPointLights = 0;

uniform float far_plane;

uniform vec3 cameraPos = vec3(1.);

uniform bool debug = false;
uniform bool highlightEdges = false;	//if the mesh is selected we draw it again but with edges only, in black


vec4 currentColor = highlightEdges ? vec4(0.f, 0.f, 0.f, 1.f) : color;


vec3 getAddedPointLightColor(int i)
{
	vec3 lightDir = normalize(gs_pos - pointLightsPositions[i]);

	//diffuse
	float diffuseLight = max(dot(gs_normal, -lightDir), 0.0);

	//specular
	vec3 viewDirection = normalize(gs_pos - cameraPos);
	vec3 reflectDirection = reflect(-lightDir, gs_normal);

	float specularLight = pow(max(dot(viewDirection, reflectDirection), 0.0), shininess);

	//result
	vec4 addedPtLightColor = (currentColor * vec4(pointLightsColors[i], 1.)) * (diffuseLight)
							+(currentColor * vec4(pointLightsColors[i], 1.)) * specularLight * 0.5f;

	return addedPtLightColor.xyz;
}


vec3 getAddedSunLightColor(int i)
{

	//diffuse
	float diffuseLight = max(dot(gs_normal, -sunLightsDirections[i]), 0.0);

	//specular
	vec3 viewDirection = normalize(gs_pos - cameraPos);
	vec3 reflectDirection = reflect(-sunLightsDirections[i], gs_normal);

	float shininess = 32;
	float specularLight = pow(max(dot(viewDirection, reflectDirection), 0.0), shininess);

	//result
	vec4 addedSunLightColor = (currentColor * vec4(sunLightsColors[i], 1.)) * (diffuseLight)
							 +(currentColor * vec4(sunLightsColors[i], 1.)) * specularLight * 0.5f;

	return addedSunLightColor.xyz;
}

vec3 getAddedSSColor(int i)
{
	vec3 fragToLight = pointLightsPositions[i] - gs_pos;
	float closestDepth = texture(lightDepthCubemap[i], -fragToLight).r;
    // it is currently in linear range between [0,1]. Re-transform back to original value
    closestDepth *= far_plane;
	vec3 normalAtMeshEntry = texture(lightNormalDataCubemap[i], -fragToLight).rgb;
	// values were set between [0;1] to fit channel, but they could be between [-1,1] (for direction)
	normalAtMeshEntry = (normalAtMeshEntry*2 -1); 
    // now get current linear depth as the length between the fragment and light position
    float currentDepth = length(fragToLight);

	float distToLightedPoint = currentDepth - closestDepth;
	
	float bias = 0.05;
	//if not in shadow, it isn't the subsurface scattering that we're computing
	if(currentDepth - bias < closestDepth)
		return vec3(0);

	float directlyLighted = currentDepth - bias < closestDepth ? 1.0: 0.0;

	vec4 ssLight = vec4(pointLightsColors[i],1) * max(dot((normalAtMeshEntry), normalize(pointLightsPositions[i]-gs_pos)), 0.0)	//take the normal of entry in the mesh into account
						* currentColor //absorbed by the entry in the mesh
						* subsurfaceColor * 1.0/(density * pow(distToLightedPoint +1 , 2)); // absorbed by the distance² spent in the mesh 

	return ssLight.xyz;
}


void main()
{
	vec3 pointLightAddedColor; 
	vec3 ssAddedColor;
	for (int i = 0; i < nbOfPointLights && i<6; i++)
	{
		pointLightAddedColor += getAddedPointLightColor(i);
		ssAddedColor += getAddedSSColor(i);
	}
	vec3 sunLightAddedColor; 
	for (int i = 0; i < nbOfSunLights && i<10; i++)
	{
		sunLightAddedColor += getAddedSunLightColor(i);
	}

	vec4 ambientLightAddedColor = currentColor * vec4(ambientLight, currentColor[3]);

//	//use gs_color[3] to keep the transparent value of the base color on the output
	fs_color = ambientLightAddedColor
				+ vec4(sunLightAddedColor, currentColor[3])
				+ vec4(pointLightAddedColor, currentColor[3])
				+ vec4(ssAddedColor, currentColor[3]);

	
	//debug
	if(debug)
	{
		fs_color = vec4(gs_normal,1);
	}
}