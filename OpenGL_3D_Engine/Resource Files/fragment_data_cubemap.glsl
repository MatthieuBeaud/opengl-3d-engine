#version 440 core
in vec4 fragPosWorld;
in vec3 gs_normal;

out vec4 out_normal;

uniform vec3 lightPos;
uniform float far_plane;

void main()
{
    // get distance between fragment and light source
    float lightDistance = length(fragPosWorld.xyz - lightPos);
    
    // map to [0;1] range by dividing by far_plane
    lightDistance = lightDistance / far_plane;
    
    // write this as modified depth
    gl_FragDepth = lightDistance;
    out_normal = vec4((gs_normal+1)/2, 1);
}  