#version 440

out vec4 fs_color;

in vec2 vs_texCoords;

uniform sampler2D depthMap;
uniform float near_plane;
uniform float far_plane;
uniform bool orthographicDepth;

// required when using a perspective projection matrix
float LinearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0; // Back to NDC 
    return (2.0 * near_plane * far_plane) / (far_plane + near_plane - z * (far_plane - near_plane));	
}

void main()
{             
      float depthValue = texture(depthMap, vs_texCoords).r;
      fs_color = (1- int(orthographicDepth))* vec4(vec3(LinearizeDepth(depthValue) / far_plane), 1.0) // perspective
                    + int(orthographicDepth)*vec4(vec3(depthValue), 1); // orthographic
}

