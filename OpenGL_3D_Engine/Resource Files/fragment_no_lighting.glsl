#version 440

uniform vec4 color = vec4(0, 0, 0, 1);

uniform bool isLight = false;
uniform bool highlightEdges = false;	//if the light is selected we draw it again but with edges only, in black

out vec4 fs_color;

void main()
{
	fs_color = (isLight && highlightEdges) ? vec4(0.f, 0.f, 0.f, 1.f) : color;
}