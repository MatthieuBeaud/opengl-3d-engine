#version 440 

out vec4 FragColor;
  
in vec2 TexCoords;

uniform int colorsPerChannel;

uniform sampler2D screenTexture;

void main()
{ 
    FragColor = texture(screenTexture, TexCoords);
//  do post processing here
    float r = floor(FragColor.x * colorsPerChannel)/colorsPerChannel;
    float g = floor(FragColor.y * colorsPerChannel)/colorsPerChannel;
    float b = floor(FragColor.z * colorsPerChannel)/colorsPerChannel;
    FragColor = vec4(r,g,b,1);
}