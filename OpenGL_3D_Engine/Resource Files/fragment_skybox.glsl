#version 440

out vec4 fs_color;

in vec3 vs_texCoord;

uniform samplerCube skybox;

void main()
{
    //the images are given as +x, -x, +y, -y, +z, -z, but in a Y up world coordinate, we use a Z up 
    fs_color = texture(skybox, vs_texCoord.xzy);
}