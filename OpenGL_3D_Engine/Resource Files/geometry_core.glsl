#version 440

layout(triangles) in;

layout(triangle_strip, max_vertices = 3) out;

in vec4 vs_position[];
in vec3 vs_normal[];
in vec4 vs_position_light_space[];

out vec3 gs_normal;
out vec3 gs_pos;
out vec4 gs_pos_light_space;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform bool smoothNormal;
uniform int lightCount = 0;


vec3 computeNormalizedTriangleNormal(vec4 ptA, vec4 ptB, vec4 ptC)
{
	vec3 ba = ptA.xyz - ptB.xyz;
	vec3 bc = ptC.xyz - ptB.xyz;
	return normalize(cross(bc, ba));
}

void main()
{
	//compute the normal of the triangle from the triangles points positions

	gs_normal = computeNormalizedTriangleNormal(vs_position[0],
												vs_position[1],
												vs_position[2]);

	//for each vertex we pass on the attributes to the fragment shader
	gs_pos = vs_position[0].xyz;
	gs_normal = smoothNormal?vs_normal[0]:computeNormalizedTriangleNormal(vs_position[0], vs_position[1], vs_position[2]);
	gs_pos_light_space = vs_position_light_space[0];
	gl_Position = gl_in[0].gl_Position;
	EmitVertex();

	gs_pos = vs_position[1].xyz;
	gs_normal = smoothNormal?vs_normal[1]:computeNormalizedTriangleNormal(vs_position[0], vs_position[1], vs_position[2]);
	gs_pos_light_space = vs_position_light_space[1];
	gl_Position = gl_in[1].gl_Position;
	EmitVertex();

	gs_pos = vs_position[2].xyz;
	gs_normal = smoothNormal?vs_normal[2]:computeNormalizedTriangleNormal(vs_position[0], vs_position[1], vs_position[2]);
	gs_pos_light_space = vs_position_light_space[2];
	gl_Position = gl_in[2].gl_Position;
	EmitVertex();

	EndPrimitive();

}