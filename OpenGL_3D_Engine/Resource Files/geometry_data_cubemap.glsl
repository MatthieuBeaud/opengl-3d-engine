#version 440 core
layout (triangles) in;
layout (triangle_strip, max_vertices=18) out;

in vec3 vs_normal[];

out vec3 gs_normal;
out vec4 fragPosWorld;

uniform mat4 faceViewProjMatrices[6]; //projviewMatrices in each cubemap direction
uniform vec3 lightPos;
uniform bool smoothNormal;

vec3 computeNormalizedTriangleNormal(vec4 ptA, vec4 ptB, vec4 ptC)
{
	vec3 ba = ptA.xyz - ptB.xyz;
	vec3 bc = ptC.xyz - ptB.xyz;
	return -normalize(cross(ba, bc));
}

void main()
{
    for(int face = 0; face < 6; ++face)
    {
        gl_Layer = face; // built-in variable that specifies to which face we render.
        for(int i = 0; i < 3; ++i) // for each triangle vertex
        {
            fragPosWorld = gl_in[i].gl_Position;
            gs_normal = smoothNormal?vs_normal[i]:computeNormalizedTriangleNormal(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_in[2].gl_Position);
            gl_Position = faceViewProjMatrices[face] * fragPosWorld;
            EmitVertex();
        }    
        EndPrimitive();
    }
}  