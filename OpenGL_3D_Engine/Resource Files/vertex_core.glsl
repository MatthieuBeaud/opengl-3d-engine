#version 440

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;

out vec4 vs_position;
out vec3 vs_normal;
out vec4 vs_position_light_space;

uniform mat4 lightProjMat;
uniform mat4 lightViewMat;
uniform int lightCount = 0;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;


void main()
{
	vs_position = modelMatrix * vec4(vertex_position, 1.f);
	vs_normal =  normalMatrix * normalize(vertex_normal);
	vs_position_light_space = lightProjMat * lightViewMat * modelMatrix * vec4(vertex_position, 1.f);
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertex_position, 1.f);
}