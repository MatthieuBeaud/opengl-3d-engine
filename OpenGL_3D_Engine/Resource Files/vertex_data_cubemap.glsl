#version 440 core
layout (location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;

out vec3 vs_normal;

uniform mat4 modelMatrix;
uniform mat3 normalMatrix;
uniform vec3 lightPos;

void main()
{
    gl_Position = modelMatrix * vec4(vertex_position, 1.0);
    vs_normal = normalMatrix * normalize(vertex_normal);
}  