#version 440

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;
layout(location = 2) in vec2 vertex_texCoords;

out vec2 vs_texCoords;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

void main()
{
    vs_texCoords = vertex_texCoords;
    gl_Position =vec4(2*vertex_position, 1.f);
}