#version 440

layout(location = 0) in vec3 vertex_position;

uniform bool isLight = false;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

void main()
{
	if (isLight)
	{
		gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertex_position, 1.f);
	}
	else
	{
		gl_Position = projectionMatrix * viewMatrix * vec4(vertex_position, 1.f);
	}
}