#version 440 


layout (location = 0) in vec2 vertex_position;
layout (location = 1) in vec2 vertex_texcoords;

out vec2 TexCoords;

void main()
{
    gl_Position = vec4(vertex_position.x, vertex_position.y, 0.0, 1.0); 
    TexCoords = vertex_texcoords;
}  