#version 440
layout(location = 0) in vec3 vertex_pos;

out vec3 vs_texCoord;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix; //to scale the cube up to a big enough size

void main()
{
    vs_texCoord = vec3(modelMatrix * vec4(vertex_pos,1));
    vec4 pos = projectionMatrix * viewMatrix * modelMatrix * vec4(vertex_pos, 1.0);
    gl_Position = pos.xyww;
}