#include "Application.h"
#include "Core/VertexArray.h"
#include "Core/VertexBuffer.h"
#include "Core/IndexBuffer.h"
#include "Core/Vertex.h"
#include "Core/Shader.h"
#include "Core/Camera.h"
#include "Core/Mesh.h"
#include "Core/Grid.h"
#include "Core/Axis.h"
#include "Core/DepthMap.h"
#include "Utils/MathsHelper.h"
#include "UI/ImGuiMenus.h"
#include "Core/Vertex2d.h"
#include <STB/stb_image.h>

Application::Application()
{
    middleMousePressed = false;
    xPos, yPos = 0.f;

    glm::vec3 worldUp = glm::vec3(0.f, 0.f, 1.f);
    glm::vec3 camPosition = glm::vec3(4.f, -10.f, 3.f);
    glm::vec3 camLookAtPosition = glm::vec3(0.f);
    float camFOV = 60.f;
    float camNearPlane = 0.01f; //min view distance
    float camFarPlane = 50; //max view distance
    float camAspectRatio = static_cast<float>(WINDOW_START_WIDTH) / WINDOW_START_HEIGHT;

    camera = Camera(camPosition, camLookAtPosition, worldUp, camAspectRatio, camFOV, camNearPlane, camFarPlane);

    scene = Scene("My Scene");    //empty world named my world
}

Application::~Application()
{
    
}

bool Application::initOpenGlAndWindow()
{
    /* Initialize the library */
    if (!glfwInit())
        return false;

    //using the core profile on version 4.4
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    //set window resizable
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    window = glfwCreateWindow(WINDOW_START_WIDTH, WINDOW_START_HEIGHT, "OpenGL Application", NULL, NULL);
    if (!window)
    {
        cout << "couldn't create window" << endl;
        glfwTerminate();
        return false;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    //init GLEW (needs window and openGL context)
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        cerr << "Error: glewInit() failed" << endl;
        glfwTerminate();
    }

    cout << glGetString(GL_VERSION) << endl; //should be 4.4

    //OpenGL Options
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    /* //only see the shaders from one side, (black from the other side)
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    */
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //to fill the faces, to see only the edges use GL_LINE

    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
    glPointSize(8.0);

    return true;
}

bool Application::Init()
{
    if (!initOpenGlAndWindow())
    {
        return false;
    }
    ImGuiMenus::Init(window);

    // Set the window callbacks
    glfwSetWindowUserPointer(window, this);

    //set frame buffer size 
    int fbW, fbH;
    glfwGetFramebufferSize(window, &fbW, &fbH);
    renderer.OnWindowResized(fbW, fbH);
    frameWidth = fbW;
    frameHeight = fbH;

    //window resize
    glfwSetFramebufferSizeCallback(window, OnWindowResizedCallback);

    //Key Input
    glfwSetKeyCallback(window, OnKeyPressedCallback);

    //Mouse Movement
    glfwSetCursorPosCallback(window, OnCursorPositionChangedCallback);
    //Mouse inputs
    glfwSetMouseButtonCallback(window, OnMouseButtonPressedCallback);
    //Mouse Scroll
    glfwSetScrollCallback(window, OnMouseScrolledCallback);

    return true;
}

void Application::Run()
{
    renderer.Init();

    Mesh* suzanne = new Mesh("./Resource Files/obj files/suzanneSmooth.obj", "Suzanne");
    *(suzanne->GetRotationPtr()) = glm::vec3(90, 0, 0);
    *(suzanne->GetPositionPtr()) = glm::vec3(0, -5, 1);
    scene.AddObject(suzanne);

    scene.AddObject(new PointLight("PointLight", glm::vec3(1.f), glm::vec3(0.f, 0.f, 0.f), 10));
    scene.AddObject(new PointLight("PointLight", glm::vec3(1.f), glm::vec3(0.f, 0.f, 3.f), 10));

    //need to be given as if we were in a Y up world, but we use a Z up world
    //as: right, left, top, bot, front, back
    //we switch back to Z up world in the skybox shader
    vector<string> skyboxFacesPath =
    {
        "./Resource Files/img/skybox0/+x.png",
        "./Resource Files/img/skybox0/-x.png",
        "./Resource Files/img/skybox0/+z.png",
        "./Resource Files/img/skybox0/-z.png",
        "./Resource Files/img/skybox0/+y.png",
        "./Resource Files/img/skybox0/-y.png",        
       /* "./Resource Files/img/skybox/right.jpg",
        "./Resource Files/img/skybox/left.jpg",
        "./Resource Files/img/skybox/top.jpg",
        "./Resource Files/img/skybox/bottom.jpg",
        "./Resource Files/img/skybox/front.jpg",
        "./Resource Files/img/skybox/back.jpg",*/
    };

    scene.SetSkybox(skyboxFacesPath);


    while (!glfwWindowShouldClose(window))
    {
        /* Poll for and process events */
        glfwPollEvents();

        renderer.Render(scene, camera, settings);

        //ImGui part
        ImGuiMenus::NewFrame();

        ImGuiMenus::MainParameterWindow(renderer.GetBackgroundColorPtr(), renderer.GetAmbientLightPtr(), camera.GetFOVptr(),
            renderer.GetGridPtr(), renderer.GetAxisPtr(), settings.showPickingRay, renderer.GetColorsPerChannelPtr());

        if (showHelpWindow)
        {
            ImGuiMenus::HelpsWindow();
        }

        ImGuiMenus::WorldWindow(scene);

        ImGuiMenus::NewObjectWindow(window, scene);

        ImGuiMenus::TimelineWindow(animationSystem, scene);

        ImGuiMenus::Render();

        //End render, Swap front and back buffers
        glfwSwapBuffers(window);
        glFlush();
    }

    //end of program
    ImGuiMenus::ShutDown();

    glfwTerminate();
}



//take in the mouse position from the top left and returns the ray direction in world coordinates
glm::vec3 Application::mousePickingDirection(double mouseX, double mouseY)
{
    //normalize device Coordinates
    float x = (2.0f * (float)mouseX) / frameWidth - 1.0f;
    float y = 1.0f - (2.0f * (float)mouseY) / frameHeight;
    float z = 1.0f;
    glm::vec3 ray_nds = glm::vec3(x, y, z);
    //homogeneous clip coordinates
    glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);
    //eye coordinates
    glm::vec4 ray_eye = glm::inverse(camera.ComputeProjectionPerspectiveMatrix()) * ray_clip;
    ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);
    //world coordinates
    glm::vec3 ray_world = (glm::inverse(camera.ComputeViewMatrix()) * ray_eye);
    // don't forget to normalise the vector at some point
    ray_world = glm::normalize(ray_world);
    return ray_world;
}


//callback methods
void Application::OnWindowResizedCallback(GLFWwindow* window, int fbW, int fbH)
{
    auto app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));

    glViewport(0, 0, fbW, fbH);
    app->camera.SetAspectRatio(static_cast<float>(fbW) / fbH);
    app->frameWidth = fbW;
    app->frameHeight = fbH;
    app->renderer.OnWindowResized(fbW, fbH);
}
                  
                  
void Application::OnKeyPressedCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    auto app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));

    if (ImGui::GetIO().WantTextInput) {
        //cout << "ImGui expects a text input, so key call back are disabled" << endl;
        return;
    }

    if (key == GLFW_KEY_ESCAPE and action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    if (key == GLFW_KEY_E and action == GLFW_PRESS)
    {
        cout << "E pressed: ";
        app->settings.edgesOnly = !app->settings.edgesOnly;
        cout << "edges only "<< (app->settings.edgesOnly?"activated":"desactivated") << endl;
    }
    if (key == GLFW_KEY_T and action == GLFW_PRESS)
    {
        cout << "T pressed, camera type switched to ";
        app->camera.SwitchType();
        cout << app->camera.GetTypeStr() << endl;
    }
    if (key == GLFW_KEY_D and action == GLFW_PRESS)
    {
        cout << "D pressed: ";
        app->settings.debug = !app->settings.debug;
        cout<< "debug mode is "<< (app->settings.debug?"On":"Off") << endl;
    }
    if (key == GLFW_KEY_N and action == GLFW_PRESS)
    {
        cout << "N pressed: ";
        app->settings.useSmoothNormals = !app->settings.useSmoothNormals;
        cout << "now using " << (app->settings.useSmoothNormals ? "smooth" : "face") << " normals" << endl;
    }
    if (key == GLFW_KEY_R and action == GLFW_PRESS)
    {
        cout << "R pressed: Reloading Shaders" << endl;
        app->renderer.ReloadShaders();
    }
    if ((key == GLFW_KEY_DELETE || key== GLFW_KEY_X) and action == GLFW_PRESS)
    {
        if (app->scene.DeleteSelectedObject())
            cout << "Delete / X pressed: deleting selected object" << endl;
        else
            cout << "Delete / X pressed: no object currently selected" << endl;
    }
    if (key == GLFW_KEY_F and action == GLFW_PRESS)
    {
        cout << "F pressed: ";
        if (app->scene.GetSelectedObjectIndex() == -1)
        {
            cout << "no object selected" << endl;
            return;
        }

        glm::vec3 newLookAtPos = glm::vec3(0.f);
        Object* selectedObj = app->scene.GetSelectedObject();

        if (selectedObj->GetType() == ObjectType::Mesh)
        {
            newLookAtPos = *(dynamic_cast<Mesh*>(selectedObj)->GetPositionPtr());
            cout << "focusing on the selected mesh" << endl;
        }
        if (selectedObj->GetType() == ObjectType::Light)
        {
            if (dynamic_cast<Light*>(selectedObj)->GetLightType() == LightType::Point)
            {
                newLookAtPos = *(dynamic_cast<PointLight*>(selectedObj)->GetPositionPtr());
                cout << "focusing on the selected light" << endl;
            }
            else
            {
                cout << "can't focus on a sun / directionnal light" << endl;
            }
        }
        app->camera.SetLookAt(newLookAtPos);
    }

    else {
        if(action == GLFW_PRESS) // show inputs
            cout << "Click on the small \" Show Help\" button in the UI to show command keys" << endl;
    }
}
                  
                  
void Application::OnCursorPositionChangedCallback(GLFWwindow* window, double xpos, double ypos)
{
    auto app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));

    if (app->middleMousePressed)
    {
        app->camera.RotateAroundLookAtPoint(xpos - app->xPos, ypos - app->yPos);
        glfwGetCursorPos(window, &app->xPos, &app->yPos);
    }
}
                  
                  
void Application::OnMouseButtonPressedCallback(GLFWwindow* window, int button, int action, int modifiers)
{
    auto app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));

    if ((button == GLFW_MOUSE_BUTTON_MIDDLE || (button == GLFW_MOUSE_BUTTON_LEFT && modifiers == GLFW_MOD_ALT)) && action == GLFW_PRESS)
    {
        glfwGetCursorPos(window, &app->xPos, &app->yPos);
        app->middleMousePressed = true;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
    if ((button == GLFW_MOUSE_BUTTON_MIDDLE || button == GLFW_MOUSE_BUTTON_LEFT) && action == GLFW_RELEASE)
    {
        app->middleMousePressed = false;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && modifiers == 0 && !ImGuiMenus::WantCaptureMouse())
    {
        glfwGetCursorPos(window, &app->xPos, &app->yPos);

        glm::vec3 rayDirection = app->mousePickingDirection(app->xPos, app->yPos);
        //draw the ray
        app->settings.pickingRay = new Mesh(Line(app->camera.GetPos(), rayDirection, 100), "Ray");
        app->settings.pickingRay->SetColor(glm::vec4(1., 0., 0., 1.));
        //select the objects touched by the ray
        app->scene.SelectObj(app->scene.RayFirstObjectTouched(app->camera.GetPos(), rayDirection));
    }
}
                  
                  
void Application::OnMouseScrolledCallback(GLFWwindow* window, double xoffset, double yoffset) //yoffset = wheel movement, xoffset use for tactile pads etc
{
    auto app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));

    app->camera.Zoom(yoffset);
}

