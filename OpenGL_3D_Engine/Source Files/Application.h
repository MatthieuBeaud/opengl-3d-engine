#pragma once

#include "libs.h"

#include "Core/Color.h"
#include "Core/Scene.h"
#include "Core/Camera.h"
#include "Core/Renderer.h"
#include "Core/AnimationSystem.h"


class Application 
{
	const int WINDOW_START_WIDTH = 2400;
	const int WINDOW_START_HEIGHT = 1200;

private:
	GLFWwindow* window = nullptr;

	int frameWidth = 0, frameHeight = 0;

	bool middleMousePressed;
	double xPos, yPos;

	RenderSettings settings
	{
		false,//edgesOnly;
		false,//useOrtho;
		false,//useSmoothNormals;
		false,//debug;
		false,//showPickingRay;
		nullptr//pickingRay;
	};

	Camera camera;
	Scene scene;
	AnimationSystem animationSystem;
	Renderer renderer;

	glm::vec3 mousePickingDirection(double mouseX, double mouseY);

	bool initOpenGlAndWindow();

	static void OnWindowResizedCallback(GLFWwindow* window, int fbW, int fbH);
	static void OnKeyPressedCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void OnCursorPositionChangedCallback(GLFWwindow* window, double xpos, double ypos);
	static void OnMouseButtonPressedCallback(GLFWwindow* window, int button, int action, int modifiers);
	static void OnMouseScrolledCallback(GLFWwindow* window, double xoffset, double yoffset);
	

public:
	Application();
	~Application();

	bool Init();

	void Run();

	GLFWwindow* GetWindow() { return window; }
};