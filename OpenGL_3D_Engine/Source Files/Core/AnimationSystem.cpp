#include "AnimationSystem.h"
#include "Mesh.h"


AnimationSystem::AnimationSystem()
{
	endFrame = MAX_FRAME;
}

AnimationSystem::AnimationSystem(int maxFrame)
{
	endFrame = maxFrame;
}

AnimationSystem::~AnimationSystem()
{

}

void AnimationSystem::UpdateObjects()
{
	for (auto entry : keyframes)
	{
		auto object = entry.first;
		if (object->GetType() != ObjectType::Mesh) continue;
		auto mesh = static_cast<Mesh*>(object);
		*mesh->GetTransformPtr() = GetKeyframeValue(mesh, currentFrame);
	}
}

// we suppose the keyframes pair<frame, value> are sorted chronologically
Transform AnimationSystem::GetKeyframeValue(Object* const obj, int frameNum)
{
	return getInterpolatedValue(obj, frameNum);
}

std::vector<int> AnimationSystem::GetObjKeyframes(Object* const obj)
{
	std::vector<int> framesWithKeys;
	for (auto keyframe : keyframes[obj]) 
	{
		framesWithKeys.push_back(keyframe.frame);
	}
	return framesWithKeys;
}

void AnimationSystem::AddKeyframe(Object* const obj, int frameNum, Transform value)
{
	auto iterator = keyframes.find(obj);
	if (iterator == keyframes.end())
	{
		keyframes[obj] = std::vector<Keyframe>();
		keyframes[obj].push_back({ frameNum, value });
		return;
	}
	auto it = keyframes[obj].begin();
	for (; it < keyframes[obj].end(); it++)
	{
		if (it->frame == frameNum)
		{
			it->value = value;
			return;
		}
		if (it->frame > frameNum)
		{
			break;
		}
	}
	keyframes[obj].insert(it,{ frameNum, value });
}

void AnimationSystem::DeleteKeyframe(Object* const obj, int frameNum)
{
	auto iterator = keyframes.find(obj);
	if (iterator == keyframes.end()) return;

	for (size_t i=0; i< keyframes[obj].size(); i++)
	{
		if (keyframes[obj][i].frame == frameNum)
		{
			keyframes[obj].erase(keyframes[obj].begin() + i);
			return;
		}
	}
}

bool AnimationSystem::HasKeyframeAtFrame(Object* const obj, int frameNum)
{
	auto iterator = keyframes.find(obj);
	if (iterator == keyframes.end() || iterator->second.size() == 0)
	{
		//no keyframe with this obj
		return false;
	}

	auto objKeyFrames = iterator->second;
	for (size_t i = 0; i < objKeyFrames.size(); i++)
	{
		if (objKeyFrames[i].frame == frameNum)
		{
			return true;
		}
	}
	return false;
}

Transform AnimationSystem::getInterpolatedValue(Object* const obj, int frameNum)
{
	auto iterator = keyframes.find(obj);
	if (iterator == keyframes.end() || iterator->second.size() == 0)
	{
		//no keyframe with this obj
		return Transform();
	}
	auto keyframesList = iterator->second;
	if (keyframesList.size() == 1)
	{
		return keyframesList[0].value;
	}

	int previousKeyIndex = 0, nextKeyIndex = 0;
	for (size_t i = 0; i < keyframesList.size(); i++)
	{
		if (keyframesList[i].frame <= frameNum)
		{
			previousKeyIndex = i;
		}

		if (keyframesList[i].frame >= frameNum)
		{
			nextKeyIndex = i;
			break;
		}
	}

	// case if the requiered frame is after all the existing keyframes;
	if (nextKeyIndex < previousKeyIndex)
		return keyframesList[keyframesList.size() - 1].value; // the last registered keyframe

	return interpolateLinearTransform(keyframesList[previousKeyIndex].frame, keyframesList[previousKeyIndex].value,
		keyframesList[nextKeyIndex].frame, keyframesList[nextKeyIndex].value, frameNum);
}

Transform AnimationSystem::interpolateLinearTransform(int frame1, Transform transform1, int frame2, Transform transform2, int targetFrame)
{
	Transform result;
	result.position = interpolateLinear(frame1, transform1.position, frame2, transform2.position, targetFrame);
	result.rotation = interpolateLinear(frame1, transform1.rotation, frame2, transform2.rotation, targetFrame);
	result.scale = interpolateLinear(frame1, transform1.scale, frame2, transform2.scale, targetFrame);

	return result;
}

glm::vec3 AnimationSystem::interpolateLinear(int frame1, glm::vec3 value1, int frame2, glm::vec3 value2, int targetFrame)
{
	if (targetFrame == frame1) return value1;
	if (targetFrame == frame2) return value2;
	float deltaFrame = (float)frame2 - frame1;
	float weight1 = 1 - ((targetFrame - frame1) / deltaFrame);
	float weight2 = 1 - ((frame2 - targetFrame) / deltaFrame);

	return value1 * weight1 + value2 * weight2;
}
