#pragma once

#include <unordered_map>
#include <vector>
#include "Object.h"
#include "Transform.h"

#define MAX_FRAME 100

struct Keyframe
{
	int frame;
	Transform value;
};

//first naive implementation (not performance efficient)
class AnimationSystem
{
public: 
	AnimationSystem();
	AnimationSystem(int frameCount);
	~AnimationSystem();

	Transform GetKeyframeValue(Object* const obj, int frameNum);
	std::vector<int> GetObjKeyframes(Object* const obj);

	void AddKeyframe(Object* const obj, int frameNum, Transform value);
	void DeleteKeyframe(Object* const obj, int frameNum);
	bool HasKeyframeAtFrame(Object* const obj, int frameNum);
	void UpdateObjects();

	int startFrame = 0;
	int currentFrame = 0;
	int endFrame;

	bool animateOnFrameChange = true;

private:
	std::unordered_map<Object*, std::vector<Keyframe>> keyframes; //vector of keyframes sorted by frame number

	glm::vec3 interpolateLinear(int frame1, glm::vec3 val1, int frame2, glm::vec3 val2, int targetFrame);
	Transform interpolateLinearTransform(int frame1, Transform val1, int frame2, Transform val2, int targetFrame);
	Transform getInterpolatedValue(Object* const obj, int frameNum);

};


