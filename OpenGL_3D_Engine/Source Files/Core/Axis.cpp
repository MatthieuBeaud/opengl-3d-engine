#include "Axis.h"
#include "GLM/glm.hpp"

Axis::Axis()
{
	showAxis = false;

	//x axis Red
	vertices.push_back({ glm::vec3(0.f, 0.f, 0.f) });
	vertices.push_back({ glm::vec3(1.f, 0.f, 0.f) });

	//y axis Green
	vertices.push_back({ glm::vec3(0.f, 0.f, 0.f) });
	vertices.push_back({ glm::vec3(0.f, 1.f, 0.f) });

	//z axis Blue
	vertices.push_back({ glm::vec3(0.f, 0.f, 0.f) });
	vertices.push_back({ glm::vec3(0.f, 0.f, 1.f) });

	va = new VertexArray();
	vb = new VertexBuffer(vertices.data(), vertices.size() * sizeof(Vertex));
	va->AddBuffer(*vb);
	va->Unbind();
}

Axis::~Axis()
{
	delete va;
	delete vb;
}

void Axis::Render(Shader* program) const
{
	if (!showAxis) 
	{
		return;
	}
	//if we want infinite lines TODO

	glLineWidth(2);
	program->SetUniformVec4(glm::vec4(1, 0, 0, 1), "color");
	program->Use();
	va->Bind();
	glDrawArrays(GL_LINES, 0, 2);
	va->Unbind();
	program->Unuse();

	program->SetUniformVec4(glm::vec4(0, 1, 0, 1), "color");
	program->Use();
	va->Bind();
	glDrawArrays(GL_LINES, 2, 2);
	va->Unbind();
	program->Unuse();

	program->SetUniformVec4(glm::vec4(0, 0, 1, 1), "color");
	program->Use();
	va->Bind();
	glDrawArrays(GL_LINES, 4, 2);
	va->Unbind();
	program->Unuse();
	glLineWidth(1);
}
