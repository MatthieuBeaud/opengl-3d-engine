#pragma once

#include <vector>
#include "Vertex.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "Camera.h"
#include "Shader.h"

class Axis
{
private:
	bool showAxis;

	std::vector<Vertex> vertices;

	VertexArray* va;
	VertexBuffer* vb;

public:
	Axis();
	~Axis();

	inline void Show() { showAxis = true; }
	inline void Hide() { showAxis = false; }
	inline bool IsVisible() { return showAxis; }

	void Render(Shader* program) const;
};

