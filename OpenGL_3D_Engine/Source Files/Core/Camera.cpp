#include "Camera.h"

#include <iostream>


Camera::Camera()
{
	position = glm::vec3(-1.5f, 4.f, 1.5f);
	lookAt = glm::vec3(0.f);
	worldUp = glm::vec3(0.f, 0.f, 1.f);
	aspectRatio = 16.f/9.f;
	fieldOfView = 60.f;
	nearPlaneDist = 0.01f;
	farPlaneDist = 1000.f;

	type = CameraType::perspective;
	updateOrthoRanges();
}

Camera::Camera(glm::vec3 pos, glm::vec3 look_at, glm::vec3 world_up, float aspect_ratio, float field_of_view, float near_plane_dist, float far_plane_dist)
{
	position = pos;
	lookAt = look_at;
	worldUp = world_up;
	aspectRatio = aspect_ratio;
	fieldOfView = field_of_view;
	nearPlaneDist = near_plane_dist;
	farPlaneDist = far_plane_dist;

	type = CameraType::perspective;
	updateOrthoRanges();
}

Camera::~Camera()
{
}

glm::mat4 Camera::ComputeViewMatrix() const
{
	return glm::lookAt(position, lookAt, worldUp);
}

glm::mat4 Camera::ComputeProjectionPerspectiveMatrix() const
{
	return glm::perspective(glm::radians(fieldOfView), aspectRatio, nearPlaneDist, farPlaneDist);
}

glm::mat4 Camera::ComputeProjectionOrthoMatrix() const
{
	return glm::ortho(lookAt.x- orthoWidthRange /2, lookAt.x+ orthoWidthRange /2, lookAt.y - orthoHeightRange / 2, lookAt.y + orthoHeightRange / 2, -farPlaneDist, farPlaneDist);
}

glm::mat4 Camera::ComputeProjectionMatrix() const
{
	switch (type) {
		case CameraType::perspective:
			return ComputeProjectionPerspectiveMatrix();
		case CameraType::ortho:
			return ComputeProjectionOrthoMatrix();
		default: 
			return ComputeProjectionPerspectiveMatrix();
	}
}

void Camera::SetAspectRatio(float newAspectRatio)
{
	aspectRatio = newAspectRatio;
	orthoWidthRange = aspectRatio * orthoHeightRange;
}

void Camera::SetLookAt(glm::vec3 newLookAtPos)
{
	this->lookAt = newLookAtPos;
}

void Camera::SwitchType()
{
	switch (type) {
		case CameraType::ortho:
			return SwitchToPerspective();
		case CameraType::perspective:
			return SwitchToOrtho();
		default:
			return SwitchToPerspective();
	}
}

void Camera::RotateAroundLookAtPoint(double horizontalAngle, double verticalAngle)
{
	//go in local space with the lookAt point being the new center (0,0,0) of this local space
	glm::vec3 localSpacePos = position - lookAt;
	//converts to polar coordinates
	float r = glm::distance(localSpacePos, glm::vec3(0.));
	double theta = glm::degrees(acos(localSpacePos.z / r));
	double phi = glm::degrees(atan2(localSpacePos.y, localSpacePos.x));
	//modifies coordinate depending on the mouse's new position on the screen
	phi -= horizontalAngle/10;
	if (theta - verticalAngle/10 > 180) theta = 179.98f;
	else if (theta - verticalAngle/10 <= 0) theta = 0.01f;
	else theta -= verticalAngle/10;

	theta = glm::radians(theta);
	phi = glm::radians(phi);
	//re-computes the coordinates back in cartesian coordinates
	localSpacePos = r * glm::vec3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
	this->position = localSpacePos + lookAt;
}

void Camera::Zoom(double value)
{
	//go in local space with the lookAt point being the new center (0,0,0) of this local space
	glm::vec3 localSpacePos = position - lookAt;
	//converts to polar coordinates
	float r = glm::distance(localSpacePos, glm::vec3(0.));
	float theta = glm::degrees(acos(localSpacePos.z / r));
	float phi = glm::degrees(atan2(localSpacePos.y, localSpacePos.x));

	if (r - value > nearPlaneDist) {
		r -= float(value);
	}
	else {
		r = nearPlaneDist;
	}

	theta = glm::radians(theta);
	phi = glm::radians(phi);
	//re-computes the coordinates back in cartesian coordinates
	localSpacePos = r * glm::vec3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
	this->position = localSpacePos + lookAt;
	updateOrthoRanges();
}


std::string Camera::GetTypeStr() const
{
	switch (type) {
	case CameraType::ortho:
		return "ortho";
	case CameraType::perspective:
		return "perspective";
	default:
		return "ortho";
	}
}


void Camera::updateOrthoRanges()
{
	orthoHeightRange = 2 * glm::distance(position, lookAt) * tan(glm::radians(fieldOfView/2));
	orthoWidthRange = orthoHeightRange * aspectRatio;
}