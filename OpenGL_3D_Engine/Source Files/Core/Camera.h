#pragma once

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>

#include <cmath>
#include <string>

enum class CameraType {
	perspective,
	ortho
};

class Camera
{
private:
	glm::vec3 position;
	glm::vec3 lookAt; //or use look direction ? lookAt = position+lookDirection
	glm::vec3 worldUp;

	float aspectRatio;
	float fieldOfView;
	float nearPlaneDist;
	float farPlaneDist;

	CameraType type;
	float orthoWidthRange;
	float orthoHeightRange;

	void updateOrthoRanges();


public:
	Camera();
	Camera(glm::vec3 pos, glm::vec3 look_at, glm::vec3 world_up, float aspect_ratio, float field_of_view, float near_plane_dist, float far_plane_dist);
	~Camera();

	glm::mat4 ComputeViewMatrix() const;
	glm::mat4 ComputeProjectionPerspectiveMatrix() const;
	glm::mat4 ComputeProjectionOrthoMatrix() const;
	glm::mat4 ComputeProjectionMatrix() const;

	inline void SwitchToOrtho() { this->type = CameraType::ortho; }
	inline void SwitchToPerspective() { this->type = CameraType::perspective; }
	inline float DistToOrigin() const { return glm::distance(position, glm::vec3(0.f)); }

	inline float GetAspectRatio() const { return aspectRatio; }
	inline glm::vec3 GetLookAt() const { return lookAt; }
	inline glm::vec3 GetPos() const { return position; }
	inline glm::vec3 GetWorldUp() const { return worldUp; }
	inline float* GetFOVptr() { return &(this->fieldOfView); }
	inline CameraType GetType() const { return this->type; }
	std::string GetTypeStr() const;
	float getNearPlane() { return nearPlaneDist; }
	float getFarPlane() { return farPlaneDist; }

	void SetAspectRatio(float newAspectRatio);
	void SetLookAt(glm::vec3 position);
	void SwitchType();

	void RotateAroundLookAtPoint(double horizontalAngle, double verticalAngle);
	void Zoom(double value);

};

