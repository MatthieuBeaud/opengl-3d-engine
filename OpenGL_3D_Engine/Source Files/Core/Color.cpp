#include "Color.h"

Color::Color()
{
	red = 0.f;
	green = 0.f;
	blue = 0.f;
	alpha = 1.f;
}

Color::Color(unsigned int r, unsigned int g, unsigned int b, float a)
{
	red = r / 255.f;
	green = g / 255.f;
	blue = b / 255.f;
	alpha = a;
}

Color::Color(unsigned int r, unsigned int g, unsigned int b)
{
	red = r / 255.f;
	green = g / 255.f;
	blue = b / 255.f;
	alpha = 1.f;
}

Color::Color(int r, int g, int b, float a)
{
	red = r / 255.f;
	green = g / 255.f;
	blue = b / 255.f;
	alpha = a;
}

Color::Color(int r, int g, int b)
{
	red = r / 255.f;
	green = g / 255.f;
	blue = b / 255.f;
	alpha = 1.f;
}

Color::Color(float r, float g, float b)
{
	red = r;
	green = g;
	blue = b;
	alpha = 1.f;
}

Color::Color(float r, float g, float b, float a)
{
	red = r;
	green = g;
	blue = b;
	alpha = a;
}

Color::~Color()
{

}

void Color::SetRGBA(float r, float g, float b, float a)
{
	red = r;
	green = g;
	blue = b;
	alpha = a;
}

void Color::SetRGB(float r, float g, float b)
{
	red = r;
	green = g;
	blue = b;
}
