#pragma once
#include <GLM/glm.hpp>

class Color
{
private:
	float red;
	float green;
	float blue;
	float alpha;

public:
	Color();
	Color(unsigned int r, unsigned int g, unsigned int b, float a);
	Color(unsigned int r, unsigned int g, unsigned int b);
	Color(int r, int g, int b, float a);
	Color(int r, int g, int b);
	Color(float r, float g, float b);
	Color(float r, float g, float b, float a);
	~Color();

	void SetRGB(float r, float g, float b);
	void SetRGBA(float r, float g, float b, float a);

	inline float r() { return red; }
	inline float g() { return green; }
	inline float b() { return blue; }
	inline float a() { return alpha; }

	inline glm::vec4 rgba() { return glm::vec4(red, green, blue, alpha); }
	inline glm::vec3 rgb() { return glm::vec3(red, green, blue); }

};

