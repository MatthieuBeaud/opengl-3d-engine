#include "DepthMap.h"

DepthMap::DepthMap()
{
	//create the framebuffer object for rendering the depth map
	glGenFramebuffers(1, &depthMapFBO);

	//create a 2D texture that we'll use as the framebuffer's depth buffer
	glGenTextures(1, &depthMapTexture);
}

DepthMap::~DepthMap()
{
	glDeleteFramebuffers(1, &depthMapFBO);
	glDeleteTextures(1, &depthMapTexture);
}


CubemapBuffer::CubemapBuffer()
{
	glGenFramebuffers(1, &cubemapFBO);

	glGenTextures(1, &depthTexture);
	glGenTextures(1, &normalDataTexture);
}

CubemapBuffer::~CubemapBuffer()
{
	glDeleteFramebuffers(1, &cubemapFBO);
	glDeleteTextures(1, &depthTexture);
	glDeleteTextures(1, &normalDataTexture);
}

std::vector<glm::mat4> CubemapBuffer::FacesViewProjMatrices(float nearPlane, float farPlane, glm::vec3 lightPos)
{
	float aspect = (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT;
	glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), aspect, nearPlane, farPlane);


	std::vector<glm::mat4> facesProjViewMatrices;
	facesProjViewMatrices.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
	facesProjViewMatrices.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
	facesProjViewMatrices.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
	facesProjViewMatrices.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)));
	facesProjViewMatrices.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)));
	facesProjViewMatrices.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)));
	
	return facesProjViewMatrices;
}