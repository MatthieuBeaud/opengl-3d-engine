#pragma once

#include "../libs.h"

//https://learnopengl.com/Advanced-Lighting/Shadows/Shadow-Mapping

const unsigned int SHADOW_WIDTH = 4096, SHADOW_HEIGHT = 4096;

class DepthMap
{
private:

protected:

public:
	unsigned int depthMapFBO;
	unsigned int depthMapTexture;

	DepthMap();

	~DepthMap();

	unsigned int Texture() { return depthMapTexture; }
	unsigned int FBO() { return depthMapFBO; }
};


class CubemapBuffer	//cubemap
{
private:
	unsigned int cubemapFBO; //cubemapFBO
	unsigned int depthTexture;	//depthTexture
	unsigned int normalDataTexture;

	bool initialized = false;

protected:

public:
	CubemapBuffer();

	~CubemapBuffer();

	unsigned int DepthTexture() { return depthTexture; } //DepthTexture
	unsigned int NormalDataTexture() {return normalDataTexture;}
	unsigned int FBO() { return cubemapFBO; }
	inline bool isInit() { return initialized; }
	inline void SetInitialized() { initialized = true; }

	std::vector<glm::mat4> FacesViewProjMatrices(float nearPlane, float farPlane, glm::vec3 lightPos);
};

