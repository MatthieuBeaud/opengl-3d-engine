#include "Grid.h"

#include <iostream>

Grid::Grid(int size, glm::vec4 color)
{
	this->showGrid = true;
	this->size = size;
	this->color = color;
	for (int i = -size; i <= size; i++)
	{
		//line along the x axis
		vertices.push_back({ glm::vec3(size, i, 0.f) });
		vertices.push_back({ glm::vec3(-size, i, 0.f) });
		//line along the y axis
		vertices.push_back({ glm::vec3(i, size, 0.f) });
		vertices.push_back({ glm::vec3(i, -size, 0.f) });
	}

	va = new VertexArray();
	vb = new VertexBuffer(vertices.data(), vertices.size() * sizeof(Vertex));
	va->AddBuffer(*vb);
	va->Unbind();
}

Grid::~Grid()
{
	delete va;
	delete vb;
}

void Grid::Update()
{
	vb->SetVertices(vertices.data(), vertices.size() * sizeof(Vertex));
}

void Grid::Render(Shader* program)
{
	if (!showGrid)
	{
		return;
	}
	program->SetUniformVec4(color, "color");

	program->Use();
	va->Bind();
	glDrawArrays(GL_LINES, 0, GL_UNSIGNED_INT);
	va->Unbind();
	program->Unuse();
}