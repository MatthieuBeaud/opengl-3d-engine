#pragma once

#include <vector>
#include "Vertex.h"

#include "VertexArray.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"

#include "Shader.h"


class Grid
{
private: 
	bool showGrid;
	int size;
	glm::vec4 color;
	std::vector<Vertex> vertices;

	VertexArray* va;
	VertexBuffer* vb;

public: 
	Grid(int size, glm::vec4 color);
	~Grid();

	inline void Show() { showGrid = true; }
	inline void Hide() { showGrid = false; }
	inline bool IsVisible() { return showGrid; }

	inline glm::vec4* GetColorPtr() { return &color; }
	void Update();

	void Render(Shader* program);
};

