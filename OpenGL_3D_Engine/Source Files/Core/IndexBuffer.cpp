#include "IndexBuffer.h"
#include <iostream>

using namespace std;

IndexBuffer::IndexBuffer()
{
	glGenBuffers(1, &id); //generates 1 buffer with the id gl_bufferID
	indexCount = 0;
}

IndexBuffer::IndexBuffer(const void* indices, const unsigned int count):
	indexCount(count)
{
	glGenBuffers(1, &id); //generates 1 buffer with the id gl_bufferID
	Bind();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count*sizeof(GLuint), indices, GL_STATIC_DRAW); //sets the data with the vertices
																			//use GL_DYNAMIC_DRAW if indices change often
}

IndexBuffer::~IndexBuffer()
{
	glDeleteBuffers(1, &id);
}


void IndexBuffer::SetIndices(const void* indices, const unsigned int count)
{
	indexCount = count;
	Bind();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count*sizeof(GLuint), indices, GL_STATIC_DRAW);
}

void IndexBuffer::Bind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
}

void IndexBuffer::Unbind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}