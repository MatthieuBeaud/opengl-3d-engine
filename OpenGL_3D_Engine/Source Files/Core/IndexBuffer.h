#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>


class IndexBuffer
{
private: 
	GLuint id;
	GLsizei indexCount;

public:
	IndexBuffer();
	IndexBuffer(const void* indices, const unsigned int count);
	~IndexBuffer();

	void SetIndices(const void* indices, const unsigned int count);
	void Bind() const;
	void Unbind() const;

	GLsizei GetIndexCount() { return indexCount; }
	GLuint ID() { return id; }

};

