#include "Light.h"
#include <iostream>

using namespace std;

#pragma region Light

Light::Light(string name, LightType light_type, glm::vec3 color):
	Object(name, ObjectType::Light)
{
	this->lightType = light_type;
	this->color = color;
}

Light::~Light()
{

}

#pragma endregion


#pragma region SunLight
SunLight::SunLight(string name, glm::vec3 color, float theta, float phi, float strength):
	Light(name, LightType::Sun ,color)
{
	this->theta = theta;
	this->phi = phi;
	this->strength = strength;
}

SunLight::~SunLight()
{

}

glm::vec3 SunLight::GetDirectionVector() const
{
	float t = glm::radians(theta);
	float p = glm::radians(phi);
	return glm::normalize(glm::vec3(sin(t) * cos(p), sin(t) * sin(p), cos(t)));
}

void SunLight::Render(Shader* program, Shader* lightProgram, unsigned int lightNum)
{
	program->SetUniformVec3(GetDirectionVector(), ("sunLightsDirections["+ std::to_string(lightNum)+"]").c_str());
	program->SetUniformVec3(*GetColorPtr(), ("sunLightsColors["+ std::to_string(lightNum)+"]").c_str());
}

#pragma endregion


#pragma region PointLight

PointLight::PointLight(string name, glm::vec3 color, glm::vec3 position, float power) :
	Light(name, LightType::Point, color), sphere(Sphere(), "lightSphere"), cubemapBuffer()
{
	this->position = position;
	this->power = power;
	*(sphere.GetScalePtr()) = glm::vec3(0.2f);
}

PointLight::~PointLight()
{

}

void PointLight::Select()
{
	isSelected = true;
	sphere.Select();
}

void PointLight::UnSelect()
{
	isSelected = false;
	sphere.UnSelect();
}

void PointLight::Render(Shader* program, Shader* lightProgram, unsigned int lightNum)
{
	program->SetUniformVec3(position, ("pointLightsPositions[" + std::to_string(lightNum) + "]").c_str());
	program->SetUniformVec3(*GetColorPtr(), ("pointLightsColors[" + std::to_string(lightNum) + "]").c_str());

	if (lightProgram == nullptr)
		return;
	//update the sphere color with the light color in case this one changed 
	//(can't share the same color ptr pointer since one is vec3 and the other vec4)
	*(sphere.GetPositionPtr()) = position;
	sphere.SetColor(glm::vec4(color, 1.0));
	lightProgram->SetUniformInt(GL_TRUE, "isLight");
	sphere.Render(lightProgram);
	lightProgram->SetUniformInt(GL_FALSE, "isLight"); //set it back for other elements that us the same program
}

#pragma endregion
