#pragma once

#include "Mesh.h"
#include "Shader.h"
#include "DepthMap.h"

enum class LightType 
{
	Point, 
	Sun, 
	Spot
};


class Light: public Object
{
private:
	LightType lightType;

protected: 
	glm::vec3 color;
	Light(string name, LightType light_type, glm::vec3 color);

public:
	virtual ~Light();

	inline LightType GetLightType() { return lightType; }
	inline glm::vec3* GetColorPtr() { return &color; }

	virtual void Render(Shader* program, Shader* lightProgram, unsigned int lightNum) {}
};


class SunLight : public Light
{
private:
	float theta;
	float phi;
	float strength;

public:
	SunLight(string name, glm::vec3 color, float theta, float phi, float strength);
	~SunLight();

	glm::vec3 GetDirectionVector() const;
	inline float* GetThetaPtr() { return &theta; }
	inline float* GetPhiPtr() { return &phi; }

	virtual void Render(Shader* program, Shader* lightProgram, unsigned int lightNum);
};


class PointLight : public Light
{
private:
	glm::vec3 position;
	float power;
	Mesh sphere;

	CubemapBuffer cubemapBuffer;

public:
	PointLight(string name, glm::vec3 color, glm::vec3 position, float power);
	~PointLight();

	inline glm::vec3* GetPositionPtr() { return &position; }
	inline glm::vec3 GetPosition() const { return position; }
	inline CubemapBuffer* GetCubemapBufferPtr() { return &cubemapBuffer; }

	bool IsIntersectedByRay(glm::vec3 rayStart, glm::vec3 rayDirection, float& distanceFromRayStart) 
	{ 
		return sphere.IsIntersectedByRay(rayStart, rayDirection, distanceFromRayStart); 
	}

	virtual void Render(Shader* program, Shader* lightProgram, unsigned int lightNum);

	virtual void Select() override;
	virtual void UnSelect() override;
};