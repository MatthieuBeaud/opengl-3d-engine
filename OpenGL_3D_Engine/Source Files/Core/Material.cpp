#include "Material.h"


Material::Material()
{
	color = glm::vec4(.7f, .7f, .7f, 1.f);
	shininess = 32;
}

Material::Material(glm::vec4 c)
{
	color = c;
	shininess = 32;
}

Material::Material(glm::vec4 c, int s)
{
	this->color = c;
	shininess = s;
}

Material::~Material()
{

}