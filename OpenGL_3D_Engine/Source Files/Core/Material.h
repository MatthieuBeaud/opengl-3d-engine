#pragma once

#include <GLM/glm.hpp>



class Material
{

private:
	glm::vec4 color;
	int shininess;
	//texture 2D

public: 
	Material();
	Material(glm::vec4 c);
	Material(glm::vec4 c, int s);
	~Material();

	inline glm::vec4* GetColorPtr() { return &color; }
	inline int* GetShininessPtr() { return &shininess;}

};

