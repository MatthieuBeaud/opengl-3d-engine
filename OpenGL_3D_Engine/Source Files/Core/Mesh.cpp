#include "Mesh.h"

#include <iostream>
#include <fstream>
#include "../Utils/MathsHelper.h"
#include <map>


Mesh::Mesh(Vertex* verticesData, const unsigned nbOfVertices, GLuint* indicesData, const unsigned nbOfIndices, string name):
	Object(name, ObjectType::Mesh)
{
	vertices = std::vector<Vertex>(verticesData, verticesData + nbOfVertices);
	indices = std::vector<GLuint>(indicesData, indicesData + nbOfIndices);

	subsurfaceColor = defautltSSColor;
	density = 1;

	//position in the world
	modelMatrix = glm::mat4(1.f);
	normalMatrix = glm::mat3(1.f);

	va = new VertexArray();
	vb = new VertexBuffer(vertices.data(), vertices.size()*sizeof(Vertex));
	ib = new IndexBuffer(indices.data(), indices.size());
	va->AddBuffer(*vb);
	va->Unbind();
}


Mesh::Mesh(Primitive primitive, string name) :
	Object(name, ObjectType::Mesh)
{
	vertices = primitive.GetVertices();
	indices = primitive.GetIndices();

	subsurfaceColor = defautltSSColor;
	density = 1;

	//position in the world
	modelMatrix = glm::mat4(1.f);
	normalMatrix = glm::mat3(1.f);

	va = new VertexArray();
	vb = new VertexBuffer(vertices.data(), vertices.size() * sizeof(Vertex));
	ib = new IndexBuffer(indices.data(), indices.size());
	va->AddBuffer(*vb);
	va->Unbind();
}

Mesh::Mesh(const char* objFileName, string name) :
	Object(name, ObjectType::Mesh)
{
	bool success = loadObj(objFileName, vertices, indices);

	if(success && !vertices.empty() && !indices.empty())
	{
		subsurfaceColor = defautltSSColor;
		density = 1;

		//position in the world
		modelMatrix = glm::mat4(1.f);
		normalMatrix = glm::mat3(1.f);


		va = new VertexArray();
		vb = new VertexBuffer(vertices.data(), vertices.size() * sizeof(Vertex));
		ib = new IndexBuffer(indices.data(), indices.size());
		va->AddBuffer(*vb);
		va->Unbind();
	}
	else
	{
		cerr << "error loading the .obj" << endl;
		throw exception("impossible to load from .obj file");
	}
}

Mesh::~Mesh()
{
	delete va;
	delete vb;
	delete ib;
}

void Mesh::Update()
{
	vb->SetVertices(vertices.data(), vertices.size() * sizeof(Vertex));
}


bool Mesh::IsIntersectedByRay(glm::vec3 rayStart, glm::vec3 rayDirection, float& distanceFromRayStart)
{
	updateModelMatrix();
	vector<glm::vec3> triangleVertices;
	float x0, y0, z0, x, y, z;
	x0 = rayStart.x;
	y0 = rayStart.y;
	z0 = rayStart.z;
	x = rayDirection.x;
	y = rayDirection.y;
	z = rayDirection.z;
	for (unsigned int triangleNum = 0; triangleNum < indices.size() / 3; triangleNum++)
	{
		triangleVertices.clear();
		for (unsigned int i = 0; i < 3; i++) 
		{
			glm::vec3 pos = vertices[indices[triangleNum * 3 + i]].position;	
			glm::vec4 posWorld =  modelMatrix* glm::vec4(pos.x, pos.y, pos.z, 1.0);
			triangleVertices.push_back(glm::vec3(posWorld.x, posWorld.y, posWorld.z));
		}
		//find 3D plane Equation
		glm::vec3 planeNormal = glm::cross(triangleVertices[1] - triangleVertices[0], triangleVertices[2] - triangleVertices[0]);
		float a, b, c, d;
		a = planeNormal.x;
		b = planeNormal.y;
		c = planeNormal.z;
		d = -(a * triangleVertices[0].x + b * triangleVertices[0].y + c * triangleVertices[0].z);
		//check if there is an intersection (normal and lineDirection not parallel)
		if (glm::cross(planeNormal, rayDirection) != glm::vec3(0)) 
		{
			//intersection 
			float t = -(a * x0 + b * y0 + c * z0 + d) / (a * x + b * y + c * z);
			glm::vec3 intersectionPt = rayStart + t * rayDirection;

			if ( t > 0 && MathsHelper::PointInTriangle(intersectionPt, triangleVertices[0], triangleVertices[1], triangleVertices[2]))
			{
				distanceFromRayStart = t;
				return true;
			}
		}
	}

	return false;
}




void Mesh::Render(Shader* program)
{
	updateModelMatrix();
	program->SetUniformMat4(modelMatrix, "modelMatrix");

	updateNormalMatrix();
	program->SetUniformMat3(normalMatrix, "normalMatrix", GL_TRUE);

	program->SetUniformVec4(*material.GetColorPtr(), "color");
	program->SetUniformVec4(subsurfaceColor, "subsurfaceColor");
	program->SetUniformInt(*material.GetShininessPtr(), "shininess");
	program->SetUniformFloat(density, "density");

	program->Use();
	va->Bind();
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	if (isSelected)
	{
		program->SetUniformInt(isSelected, "highlightEdges");
		program->Use();
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
		program->SetUniformInt(!isSelected, "highlightEdges");
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //reset to fill after drawing the edges of the model
	}
	va->Unbind();
}


void Mesh::RenderLine(Shader* program)
{
	updateModelMatrix();
	program->SetUniformMat4(modelMatrix, "modelMatrix");
	updateNormalMatrix();
	program->SetUniformMat3(normalMatrix, "normalMatrix");
	program->Use();
	va->Bind();
	glDrawElements(GL_LINES, indices.size(), GL_UNSIGNED_INT, 0);
	if (isSelected)
	{
		program->SetUniformInt(isSelected, "highlightEdges");
		program->Use();
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
		program->SetUniformInt(!isSelected, "highlightEdges");
	}
	va->Unbind();
}



void Mesh::updateModelMatrix()
{
	modelMatrix = glm::mat4(1.f);
	modelMatrix = glm::translate(modelMatrix, transform.position);
	modelMatrix = glm::rotate(modelMatrix, glm::radians(transform.rotation.x), X_AXIS);    //rotate along x axis
	modelMatrix = glm::rotate(modelMatrix, glm::radians(transform.rotation.y), Y_AXIS);    //rotate along y axis
	modelMatrix = glm::rotate(modelMatrix, glm::radians(transform.rotation.z), Z_AXIS);    //rotate along z axis
	modelMatrix = glm::scale(modelMatrix, transform.scale);
}

void Mesh::updateNormalMatrix()
{
	normalMatrix = glm::inverse(glm::mat3(modelMatrix));
}

std::vector<Vertex> Mesh::loadObjVertices(const char* objFileName)
{
	string line = "";
	string coords = "";

	std::vector<Vertex> vertices;
	float x, y, z;
	glm::vec4 defColor = glm::vec4(.7f, .7f, .7f, 1.f);

	ifstream in_file;

	in_file.open(objFileName);

	if (in_file.is_open())
	{
		while (getline(in_file, line)) {
			if (line.substr(0, 2) == "v ")
			{
				//we have a line like: "v x y z" (v for vertex)
				coords = line.substr(line.find_first_of(" ")+1);
				//we now have a line like: "x y z" so we split it around the spaces to get the values
				x = stof(coords.substr(0, coords.find_first_of(" ")));
				y = stof(coords.substr(coords.find_first_of(" ")+1, coords.find_last_of(" ")- coords.find_first_of(" ")-1));
				z = stof(coords.substr(coords.find_last_of(" ")+1));
				//cout << x << "/" << y << "/" << z <<"/"<< endl;
				vertices.push_back(Vertex({ glm::vec3(x, y, z), defColor }));
			}
		}
	}
	else {
		cout << "Error, laodObjVertices couldn't open file: " << objFileName << endl;
	}
	in_file.close();

	return vertices;
}

std::vector<GLuint> Mesh::loadObjIndices(const char* objFileName)
{
	string line = "";
	vector<string> facePts;

	std::vector<GLuint> indices;
	GLuint index;	//triangles vertex indices
	glm::vec4 defColor = glm::vec4(.7f, .7f, .7f, 1.f);

	ifstream in_file;

	in_file.open(objFileName);

	if (in_file.is_open())
	{
		while (getline(in_file, line)) {
			if (line.substr(0, 2) == "f ")
			{
				//we have a line like: "f i1/t1//n1 i2/t2/n2 i3/t3/n3 ... until the end of the face" (f for face)
				//for each face, we need to create as many triangles as needed to replace the face polygon with triangles
				while (line.find_first_of(" ") != string::npos)
				{
					line = line.substr(line.find_first_of(" ") + 1);
					facePts.push_back(line.substr(0, line.find_first_of(" ")));
				}
				for (unsigned int i = 1; i < facePts.size()-1; i++) 
				{
					string point = facePts[0];
					index = std::stoi(point.substr(0, point.find_first_of("/")));
					indices.push_back(index-1);	//in .obj indexes start at 1 and here we want them to start at 0
					for (int j = 0; j < 2; j++)
					{
						point = facePts[i + j];
						index = std::stoi(point.substr(0, point.find_first_of("/")));
						indices.push_back(index-1);  //in .obj indexes start at 1 and here we want them to start at 0
						//cout << index << "/";
					}
					//cout << " - ";
				}
				//cout << endl;
				facePts.clear();
			}
		}
	}
	else {
		cout << "Error, laodObjIndices couldn't open file: " << objFileName << endl;
	}
	in_file.close();

	return indices;
}

bool Mesh::loadObj(const char* objFileName, std::vector<Vertex>& vertices, std::vector<GLuint>& indices)
{
	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> normals;
	std::vector<std::pair<GLuint, GLuint>> positionAndNormalIndices;
	std::vector<string> faceIndices;

	GLuint vIndex;	//triangles vertex indices
	GLuint nIndex;	//triangles normal indices
	float x, y, z;

	ifstream in_file;
	string line = "";
	string coords = "";

	in_file.open(objFileName);

	if (in_file.is_open())
	{
		while (getline(in_file, line)) {
			if (line.substr(0, 2) == "v ")	// --- vertices positions
			{
				//we have a line like: "v x y z" (v for vertex)
				coords = line.substr(line.find_first_of(" ") + 1);
				//we now have a line like: "x y z" so we split it around the spaces to get the values
				x = stof(coords.substr(0, coords.find_first_of(" ")));
				y = stof(coords.substr(coords.find_first_of(" ") + 1, coords.find_last_of(" ") - coords.find_first_of(" ") - 1));
				z = stof(coords.substr(coords.find_last_of(" ") + 1));
				//cout << x << "/" << y << "/" << z <<"/"<< endl;
				positions.push_back(glm::vec3(x, y, z));
			}
			else if (line.substr(0, 3) == "vn ")	// --- vertices normals
			{
				//we have a line like: "vn x y z" (vn for vertex normal)
				coords = line.substr(line.find_first_of(" ") + 1);
				//we now have a line like: "x y z" so we split it around the spaces to get the values
				x = stof(coords.substr(0, coords.find_first_of(" ")));
				y = stof(coords.substr(coords.find_first_of(" ") + 1, coords.find_last_of(" ") - coords.find_first_of(" ") - 1));
				z = stof(coords.substr(coords.find_last_of(" ") + 1));
				//cout << x << "/" << y << "/" << z <<"/"<< endl;
				normals.push_back(glm::vec3(x, y, z));
			}
			else if (line.substr(0, 2) == "f ")	// --- faces
			{
				//we have a line like: "f i1/t1/n1 i2/t2/n2 i3/t3/n3 ... until the end of the face" (f for face)
				//for each face, we need to create as many triangles as needed to replace the face polygon with triangles
				while (line.find_first_of(" ") != string::npos)
				{
					line = line.substr(line.find_first_of(" ") + 1);
					faceIndices.push_back(line.substr(0, line.find_first_of(" ")));
				}
				for (unsigned int i = 1; i < faceIndices.size() - 1; i++)
				{
					string point = faceIndices[0];
					vIndex = std::stoi(point.substr(0, point.find_first_of("/")));
					nIndex = std::stoi(point.substr(point.find_last_of("/")+1, string::npos));
					positionAndNormalIndices.push_back(std::pair<GLuint, GLuint>(vIndex - 1, nIndex-1));	//in .obj indexes start at 1 and here we want them to start at 0
					for (int j = 0; j < 2; j++)
					{
						point = faceIndices[i + j];
						vIndex = std::stoi(point.substr(0, point.find_first_of("/")));
						nIndex = std::stoi(point.substr(point.find_last_of("/")+1, string::npos));
						positionAndNormalIndices.push_back(std::pair<GLuint, GLuint>(vIndex - 1, nIndex - 1));	//in .obj indexes start at 1 and here we want them to start at 0
						//cout << index << "/";
					}
					//cout << " - ";
				}
				//cout << endl;
				faceIndices.clear();
			}
		}
	}
	// we now have, a list of the positions, the indices, and the verticesIndices and normalIndices composing each triangle
	// we now have to create for each pair position/normal that composes a triangle, a Vertex, and an Index 
	// so we can have our vertexBuffer (that holds position and normal) and our IndexBuffer
	std::map<std::pair<GLuint, GLuint>, GLuint> existingPosNormPair;

	GLuint createdVertices = 0;
	for (auto posAndNorm : positionAndNormalIndices)
	{
		auto indexIterator = existingPosNormPair.find(posAndNorm);
		if (indexIterator == existingPosNormPair.end())
		{
			existingPosNormPair.emplace(posAndNorm, createdVertices);
			vertices.push_back(Vertex(positions[posAndNorm.first], normals[posAndNorm.second]));
			indices.push_back(createdVertices++);
		}
		else
		{
			indices.push_back(indexIterator->second);
		}
	}

	return true;
}
