#pragma once

#include <vector>
#include <list>
#include "Vertex.h"

#include "Material.h"
#include "Object.h"
#include "Transform.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include "Primitive.h"

const glm::vec4 defautltSSColor(1.0); // full opaque white

class Mesh: public Object
{
private:
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;

	Material material;
	glm::vec4 subsurfaceColor;
	float density;

	Transform transform;

	glm::mat4 modelMatrix;
	glm::mat3 normalMatrix;

	VertexArray* va;
	VertexBuffer* vb;
	IndexBuffer* ib;

	void updateModelMatrix();
	void updateNormalMatrix();

	std::vector<Vertex> loadObjVertices(const char* objFileName);
	std::vector<GLuint> loadObjIndices(const char* objFileName);
	bool loadObj(const char* objFileName, std::vector<Vertex>& vertices, std::vector<GLuint>& indices);

public:
	Mesh(Vertex* verticesData, const unsigned nbOfVertices, GLuint* indicesData, const unsigned nbOfIndices, string name);
	Mesh(Primitive primitive, string name);
	Mesh(const char* objFileName, string name);
	~Mesh();

	void Update();
	void SetColor(glm::vec4 color)
	{
		*(GetColorPtr()) = color;
	}

	inline glm::vec4* GetColorPtr() { return material.GetColorPtr(); }
	inline int* GetShininessPtr() { return material.GetShininessPtr(); }
	inline glm::vec4* GetSubsurfaceColorPtr() { return &subsurfaceColor; }
	inline float* GetDensityPtr() { return &density; }
	virtual inline Transform* GetTransformPtr() { return &transform; }
	virtual inline glm::vec3* GetPositionPtr() { return &(transform.position); }
	virtual inline glm::vec3* GetRotationPtr() { return &(transform.rotation); }
	virtual inline glm::vec3* GetScalePtr() { return &(transform.scale); }

	bool IsIntersectedByRay(glm::vec3 rayStart, glm::vec3 rayDirection, float& distanceFromRayStart);

	void Render(Shader* program);

	void RenderLine(Shader* program);
};




