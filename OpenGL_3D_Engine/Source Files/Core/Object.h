#pragma once

#include <string>
#include <GLM/glm.hpp>
#include <list>

using namespace std;

enum class ObjectType 
{
	Mesh,
	Light
};


class Object
{
protected:
	string name;
	bool isSelected;
	ObjectType type;

protected:
	Object(string name, ObjectType objType)
	{
		this->name = name;
		this->isSelected = false;
		this->type = objType;
	}

public:
	virtual ~Object() {}

	virtual void Select() { isSelected = true; }
	virtual void UnSelect() { isSelected = false; }
	inline void SetName(string newName) { this->name = newName; }

	inline string GetName() { return name; }
	inline ObjectType GetType() { return type; }

};
