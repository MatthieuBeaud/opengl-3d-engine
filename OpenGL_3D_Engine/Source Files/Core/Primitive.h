#pragma once
#include <vector>
#include "Vertex.h"
#include "../Utils/MathsHelper.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <GLM/glm.hpp>

class Primitive
{
protected: 
	Primitive() {}

public:
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;

	virtual ~Primitive() {}

	inline std::vector<Vertex> GetVertices() const { return vertices; }
	inline std::vector<GLuint> GetIndices() const { return indices; }
};


class Quad : public Primitive
{

public:
	Quad() //creates a quad (plane) width 1, height 1, centerPosition (0,0,0), normal (0,1,0)
	{
		Vertex quadVertices[] =
		{
			//Position								//Normal				        //TexCoords        
			Vertex(glm::vec3(-0.5f, 0.5f, 0.f),		glm::vec3(0.f, 1.f, 0.f),		glm::vec2(0.f, 1.f)),
			Vertex(glm::vec3(-0.5f,-0.5f, 0.f),		glm::vec3(0.f, 1.f, 0.f),		glm::vec2(0.f, 0.f)),
			Vertex(glm::vec3( 0.5f,-0.5f, 0.f),		glm::vec3(0.f, 1.f, 0.f),		glm::vec2(1.f, 0.f)),
			Vertex(glm::vec3( 0.5f, 0.5f, 0.f),		glm::vec3(0.f, 1.f, 0.f),		glm::vec2(1.f, 1.f))
		};
		vertices = std::vector<Vertex>(quadVertices, quadVertices + 4);

		GLuint quadIndices[] =
		{
			0, 1, 2,
			0, 2, 3
		};

		indices = std::vector<GLuint>(quadIndices, quadIndices + 6);
	}

	~Quad() {}
};

class Cube : public Primitive
{
public:
	Cube()
	{
		Vertex cubeVertices[] =
		{
			//4 vertices face top and their 3 normals
			//Position									//Normal				                 
			Vertex(glm::vec3(0.5f, 0.5f, 0.5f),			glm::vec3(1.f, 0.f, 0.f)),
			Vertex(glm::vec3(0.5f, 0.5f, 0.5f),			glm::vec3(0.f, 1.f, 0.f)),
			Vertex(glm::vec3(0.5f, 0.5f, 0.5f),			glm::vec3(0.f, 0.f, 1.f)),

			Vertex(glm::vec3(-0.5f, 0.5f, 0.5f),		glm::vec3(-1.f, 0.f, 0.f)),
			Vertex(glm::vec3(-0.5f, 0.5f, 0.5f),		glm::vec3(0.f, 1.f, 0.f)),
			Vertex(glm::vec3(-0.5f, 0.5f, 0.5f),		glm::vec3(0.f, 0.f, 1.f)),

			Vertex(glm::vec3(-0.5f, -0.5f, 0.5f),		glm::vec3(-1.f, 0.f, 0.f)),
			Vertex(glm::vec3(-0.5f, -0.5f, 0.5f),		glm::vec3(0.f, -1.f, 0.f)),
			Vertex(glm::vec3(-0.5f, -0.5f, 0.5f),		glm::vec3(0.f, 0.f, 1.f)),

			Vertex(glm::vec3(0.5f, -0.5f, 0.5f),		glm::vec3(1.f, 0.f, 0.f)),
			Vertex(glm::vec3(0.5f, -0.5f, 0.5f),		glm::vec3(0.f, -1.f, 0.f)),
			Vertex(glm::vec3(0.5f, -0.5f, 0.5f),		glm::vec3(0.f, 0.f, 1.f)),

			//4 vertices face bot
			Vertex(glm::vec3(0.5f, -0.5f, -0.5f),		glm::vec3(1.f, 0.f, 0.f)),
			Vertex(glm::vec3(0.5f, -0.5f, -0.5f),		glm::vec3(0.f, -1.f, 0.f)),
			Vertex(glm::vec3(0.5f, -0.5f, -0.5f),		glm::vec3(0.f, 0.f, -1.f)),

			Vertex(glm::vec3(-0.5f, -0.5f, -0.5f),		glm::vec3(-1.f, 0.f, 0.f)),
			Vertex(glm::vec3(-0.5f, -0.5f, -0.5f),		glm::vec3(0.f, -1.f, 0.f)),
			Vertex(glm::vec3(-0.5f, -0.5f, -0.5f),		glm::vec3(0.f, 0.f, -1.f)),

			Vertex(glm::vec3(-0.5f, 0.5f, -0.5f),		glm::vec3(-1.f, 0.f, 0.f)),
			Vertex(glm::vec3(-0.5f, 0.5f, -0.5f),		glm::vec3(0.f, 1.f, 0.f)),
			Vertex(glm::vec3(-0.5f, 0.5f, -0.5f),		glm::vec3(0.f, 0.f, -1.f)),

			Vertex(glm::vec3(0.5f, 0.5f, -0.5f),		glm::vec3(1.f, 0.f, 0.f)),
			Vertex(glm::vec3(0.5f, 0.5f, -0.5f),		glm::vec3(0.f, 1.f, 0.f)),
			Vertex(glm::vec3(0.5f, 0.5f, -0.5f),		glm::vec3(0.f, 0.f, -1.f)),
		};							

		vertices = std::vector<Vertex>(cubeVertices, cubeVertices + 24);

		GLuint cubeIndices[] =
		{
			2, 5, 8,    //triangles face top
			8, 11, 2,
			14, 17, 20,    //triangles  face bot
			20, 23, 14,
			0, 9, 12,	//triangles face x =.5
			12, 21, 0,
			3, 18, 15,	//triangles face x = -.5
			15, 6, 3,
			1, 22, 19,	//triangles face y = .5
			19, 4, 1,
			7, 16, 13,	//triangles face y = -.5
			13, 10, 7
		};

		/*for (int triangle = 0; triangle < 36.0 / 3; triangle++) 
		{
			glm::vec3 n = MathsHelper::NormalizedNormalOfTriangle((cubeVertices[cubeIndices[3 * triangle]]).position,
				(cubeVertices[cubeIndices[3 * triangle + 1]]).position,
				(cubeVertices[cubeIndices[3 * triangle + 2]]).position);
			cout <<"("<< n.x <<',' <<n.y << ',' <<n.z<<")"<< endl;
		}*/


		indices = std::vector<GLuint>(cubeIndices, cubeIndices + 36);
	}

	~Cube(){}
};

class Line : public Primitive
{
public:
	Line(glm::vec3 pos, glm::vec3 direction, float length)
	{
		Vertex lineVertices[] =
		{

			//Position							//Normal (line doesnt need)	
                 
			Vertex(pos,							glm::vec3(0.f, 0.f, 0.f)),
			Vertex(pos + length * direction,	glm::vec3(0.f, 0.f, 0.f)),
			
		};
		vertices = std::vector<Vertex>(lineVertices, lineVertices + 2);

		GLuint lineIndices[] =
		{
			0, 1
		};

		indices = std::vector<GLuint>(lineIndices, lineIndices + 2);
	}

	~Line() {}
};

const float PI = glm::pi<float>();

class Sphere : public Primitive {
public:
	Sphere(int segments = 16, int rings = 8)
	{
		assert(segments > 2);
		assert(rings > 2);

		float verticalAngleStep = PI / rings;
		float horizontalAngleStep = 2*PI / segments;

		float x, y, z; 
		float uvX, uvY;

		float horAngle, verAngle;
		//vertices
		for (int i = 0; i <= rings; i++)
		{
			verAngle = PI / 2 - i * verticalAngleStep;
			z = sin(verAngle); 
			for (int j = 0; j <= segments; j++)
			{
				horAngle = j * horizontalAngleStep;
				x = cos(horAngle) * cos(verAngle);
				y = sin(horAngle) * cos(verAngle);
				// position == normal because radius = 1;
				uvX = (float)j / segments;
				uvY = (float)i / rings;
				vertices.push_back(Vertex(glm::vec3(x, y, z), glm::vec3(x,y,z), glm::vec2(uvX, uvY)));
			}
		}
		//indices
		int k1, k2;
		// k1 is the index of the vertex on the upper ring,
		// k2 is the index of the vertex under k1 on the lower ring
		// both increase clock wise (right hand rule with 3rd axis going to positive z)
		// k1->k1+1
		// | /  |
		// k2->k2+1
		// triangle is k1->k2->k1+1 etc going around clock wise then from top to bottom rings
		for (int i = 0; i < rings; i++)
		{
			k1 = i * (segments + 1); // beginning of the ring
			k2 = k1 + segments + 1;	// beginning of the next ring (the one below)
			for (int j = 0; j < segments; j++, k1++, k2++)
			{
				// 2 triangles per square (formed by concecutive ring/segment intersections) excluding first and last stacks
				// k1 => k2 => k1+1
				if (i != 0)
				{
					indices.push_back(k1);
					indices.push_back(k2);
					indices.push_back(k1 + 1);
				}

				// k1+1 => k2 => k2+1
				if (i != (rings - 1))
				{
					indices.push_back(k1 + 1);
					indices.push_back(k2);
					indices.push_back(k2 + 1);
				}
			}
		}
	}

	~Sphere() {}
};

