#include "Renderer.h"
#include "../UI/ImGuiMenus.h"

Renderer::Renderer()
{
    framebufferWidth = 0;
    framebufferHeight = 0;
    currentViewMatrix = glm::identity<glm::mat4>();
    currentProjectionMatrix = glm::identity<glm::mat4>();

    renderFramebuffer = GLuint(0);
    renderTexture = GLuint(0);
    renderbuffer = GLuint(0);
    
    glClearColor(backgroundColor.r(), backgroundColor.g(), backgroundColor.b(), backgroundColor.a());
}

Renderer::Renderer(int width, int height): Renderer()
{
    framebufferWidth = width;
    framebufferHeight = height;
}

void Renderer::Init()
{
    //Shader init
    //for all world objects
    core_program.Init("./Resource Files/vertex_core.glsl", "./Resource Files/fragment_core.glsl", "./Resource Files/geometry_core.glsl");
    //for the skybox
    skybox_program.Init("./Resource Files/vertex_skybox.glsl", "./Resource Files/fragment_skybox.glsl");
    //for ui stuff (grid, mouse picking ray ...)
    no_lighting_program.Init("./Resource Files/vertex_no_lighting.glsl", "./Resource Files/fragment_no_lighting.glsl");
    depth_program.Init("./Resource Files/vertex_depth.glsl", "./Resource Files/fragment_depth.glsl");
    data_cubemap_program.Init("./Resource Files/vertex_data_cubemap.glsl", "./Resource Files/fragment_data_cubemap.glsl", "./Resource Files/geometry_data_cubemap.glsl");


    //for post processing (render to buffer, then buffer to fullscreen 2d quad as a texture)
    postprocess_program.Init("./Resource Files/vertex_postprocess.glsl", "./Resource Files/fragment_postprocess.glsl");

    grid = new Grid(10, Color(150, 150, 150).rgba());
    axis = new Axis();
    screenQuad = new Quad2d();

    // init render frame buffer and texture
    glGenFramebuffers(1, &renderFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, renderFramebuffer);
    // generate texture
    glGenTextures(1, &renderTexture);
    glBindTexture(GL_TEXTURE_2D, renderTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, framebufferWidth, framebufferHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);
    //depth test buffer
    glGenRenderbuffers(1, &renderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, framebufferWidth, framebufferHeight);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    // attach it to currently bound framebuffer object
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderTexture, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbuffer);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::ReloadShaders()
{
    core_program.Reload();
    no_lighting_program.Reload();
    depth_program.Reload();
    data_cubemap_program.Reload();
    postprocess_program.Reload();
}


Renderer::~Renderer()
{
    delete(grid);
    delete(axis);
    delete(screenQuad);
}

void Renderer::Render(Scene& scene, Camera& camera, RenderSettings settings)
{
    if (framebufferWidth == 0 || framebufferHeight == 0)    //so that the program doesn't crash when the window is minimized
    {
        return;
    }

    // update clear color if it changed in the settings window
    glClearColor(backgroundColor.r(), backgroundColor.g(), backgroundColor.b(), backgroundColor.a());

    /*update camera view and projection*/
    currentViewMatrix = camera.ComputeViewMatrix();
    currentProjectionMatrix = camera.ComputeProjectionMatrix();

    core_program.SetUniformMat4(currentViewMatrix, "viewMatrix", GL_FALSE);
    core_program.SetUniformMat4(currentProjectionMatrix, "projectionMatrix", GL_FALSE);

    core_program.SetUniformVec3(ambientLight.rgb(), "ambientLight");
    core_program.SetUniformVec3(camera.GetPos(), "cameraPos");
    core_program.SetUniformInt(settings.useSmoothNormals, "smoothNormal");
    core_program.SetUniformInt(settings.debug, "debug");

    skybox_program.SetUniformMat4(glm::mat4(glm::mat3(currentViewMatrix)), "viewMatrix", GL_FALSE); //remove the translation from the view matrix
    skybox_program.SetUniformMat4(currentProjectionMatrix, "projectionMatrix", GL_FALSE);

    data_cubemap_program.SetUniformInt(settings.useSmoothNormals, "smoothNormal");

    no_lighting_program.SetUniformMat4(currentViewMatrix, "viewMatrix", GL_FALSE);
    no_lighting_program.SetUniformMat4(currentProjectionMatrix, "projectionMatrix", GL_FALSE);

    postprocess_program.SetUniformInt(colorsPerChannel, "colorsPerChannel");


    // Render Part ------------------------------------------------------------------------------------------

    //clear window
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // we render the grid and the world to a framebuffer (ui elements like the ray and the axis don't get renderer in the framebuffer, but over the result)
    glBindFramebuffer(GL_FRAMEBUFFER, renderFramebuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear between each frame

    //render grid with a default shader (no lighting)
    grid->Render(&no_lighting_program);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //world objects / scene RENDER TO BUFFER
    scene.Render(&core_program, &data_cubemap_program, &no_lighting_program, &skybox_program,
        framebufferWidth, framebufferHeight, settings.edgesOnly, camera.getNearPlane(), camera.getFarPlane(), renderFramebuffer);

    // Render buffer to screen
    screenQuad->Render(&postprocess_program, renderTexture);

    //render axis
    axis->Render(&no_lighting_program);

    //render ray used for mousePicking
    if (settings.showPickingRay)
    {
        if (settings.pickingRay != nullptr) {
            settings.pickingRay->RenderLine(&no_lighting_program);
        }
    }

    //unbind
    glBindVertexArray(0);
}

void Renderer::OnWindowResized(int newWidth, int newHeight)
{
    glViewport(0, 0, newWidth, newHeight);
    framebufferWidth = newWidth;
    framebufferHeight = newHeight;

    glBindTexture(GL_TEXTURE_2D, renderTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, newWidth, newHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, newWidth, newHeight);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
}