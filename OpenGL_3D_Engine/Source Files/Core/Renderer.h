#pragma once
#include "Scene.h"
#include "Grid.h"
#include "Axis.h"
#include "Camera.h"
#include "Color.h"
#include "Vertex2d.h"

struct RenderSettings
{
    bool edgesOnly;
    bool useOrtho;
    bool useSmoothNormals;
    bool debug;
    bool showPickingRay;
    Mesh* pickingRay;
};

class Renderer
{
public: 
    Renderer();
    Renderer(int width, int height);
    ~Renderer();
    void Init();
    void ReloadShaders();
	void Render(Scene& scene,Camera& camera, RenderSettings settings);
    void OnWindowResized(int newWidth, int newHeight);

    Color* GetBackgroundColorPtr() { return &backgroundColor; }
    Color* GetAmbientLightPtr() { return &ambientLight; }

    Grid* GetGridPtr() { return grid; }
    Axis* GetAxisPtr() { return axis; }

    int* GetColorsPerChannelPtr() { return &colorsPerChannel; }

private: 
    //for all world objects
    Shader core_program;
    //for the skybox
    Shader skybox_program;
    //for ui stuff (grid, mouse picking ray ...)
    Shader no_lighting_program;
    Shader depth_program;
    Shader data_cubemap_program;

     
    //for post processing (render to buffer, then buffer to fullscreen 2d quad as a texture)
    Shader postprocess_program;

    int framebufferWidth;
    int framebufferHeight;

    Color backgroundColor = Color(49, 49, 49);
    Color ambientLight = Color(0, 0, 0);

    glm::mat4 currentViewMatrix;
    glm::mat4 currentProjectionMatrix;

    Grid* grid = nullptr;
    Axis* axis = nullptr;

    Quad2d* screenQuad = nullptr; //for result display from a texture

    unsigned int renderFramebuffer;
    unsigned int renderTexture;
    unsigned int renderbuffer;

    int colorsPerChannel = 255;
};

