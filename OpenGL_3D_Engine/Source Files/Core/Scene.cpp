#include "Scene.h"

#include <iostream>

bool Scene::hasAnObjectWithName(string name)
{
	for (Object* worldObj : objects)
	{
		if (worldObj->GetName()._Equal(name)) {
			return true;
		}
	}
	return false;
}

std::vector<PointLight*> Scene::getPointLights()
{
	auto objs = GetObjects();
	std::vector<PointLight*> lights;
	for (auto obj : objs)
	{
		if (obj->GetType() == ObjectType::Light && dynamic_cast<Light*>(obj)->GetLightType() == LightType::Point)
		{
			lights.push_back(dynamic_cast<PointLight*>(obj));
		}
	}
	return lights;
}

Scene::Scene()
{
	this->name = "New World";
	this->objects = std::vector<Object*>();
	this->selectedObjectIndex = -1;
	this->skybox = new Skybox();
	this->useSkybox = false;
}

Scene::Scene(std::string world_name)
{
	this->name = world_name;
	this->objects = std::vector<Object*>();
	this->selectedObjectIndex = -1;
	this->skybox = new Skybox();
	this->useSkybox = false;
}

Scene::~Scene()
{
	for(Object * worldObj: objects)
	{
		delete worldObj;
	}
	delete skybox;
}

std::vector<Light*> Scene::GetLights()
{
	auto objs = GetObjects();
	std::vector<Light*> lights;
	for (auto obj : objs)
	{
		if (obj->GetType() == ObjectType::Light)
			lights.push_back(dynamic_cast<Light*>(obj));
	}
	return lights;
}

void Scene::SelectObj(int objIndex)
{
	if (selectedObjectIndex != -1) 
	{
		objects[selectedObjectIndex]->UnSelect();
	}
	selectedObjectIndex = objIndex;
	if (selectedObjectIndex != -1)
	{
		objects[selectedObjectIndex]->Select();
	}
}

void Scene::UnselectAll()
{
	if(selectedObjectIndex != -1)
		objects[selectedObjectIndex]->UnSelect();
	selectedObjectIndex = -1;
}

int Scene::RayFirstObjectTouched(glm::vec3 rayStart, glm::vec3 rayDirection)
{
	float closestDistanceToObj= 10000.;
	float distance;
	int objIndex = -1;

	for (unsigned int i = 0; i < objects.size(); i++)
	{
		Object* obj = objects[i];
		if (obj->GetType() == ObjectType::Mesh)
		{
			Mesh* meshObject = dynamic_cast<Mesh*>(obj);
			if (meshObject->IsIntersectedByRay(rayStart, rayDirection, distance)) 
			{
				if (distance < closestDistanceToObj)
				{
					objIndex = i;
					closestDistanceToObj = distance;
				}
			}
		}
		if (obj->GetType() == ObjectType::Light)
		{
			Light* light = dynamic_cast<Light*>(obj);
			if (light->GetLightType() == LightType::Point)
			{
				PointLight* pLight = dynamic_cast<PointLight*>(light);
				if (pLight->IsIntersectedByRay(rayStart, rayDirection, distance))
				{
					if (distance < closestDistanceToObj)
					{
						objIndex = i;
						closestDistanceToObj = distance;
					}
				}
			}
		}
	}
	return objIndex;
}

void Scene::AddObject(Object* obj)
{
	if (hasAnObjectWithName(obj->GetName()))
	{
		string name = obj->GetName()+"_0";
		int i = 0;
		while (hasAnObjectWithName(name))
		{
			name = name.substr(0,name.find_last_of("_")+1) + to_string(i++);
		}
		obj->SetName(name);
	}
	objects.push_back(obj);
}

bool Scene::DeleteSelectedObject()
{
	if (selectedObjectIndex != -1)
	{
		objects.erase(objects.begin() + selectedObjectIndex);
		selectedObjectIndex = -1;
		return true;
	}
	return false;
}

void Scene::SetSkybox(std::vector<string> faceImgPaths)
{
	skybox->Init(faceImgPaths);
}

void Scene::RenderPass(Shader* program, Shader* lightProgram, int framebufferWidth, int framebufferHeight)
{
	//Renders the whole scene with draw calls for each object data
	//if lightProgram == nullptr, Light->Render will only set the lights positions and colors
	//as uniform in Program, and not render the light Spheres

	// either window or shadow dimensions
	glViewport(0, 0, framebufferWidth, framebufferHeight);

	unsigned int sunLightNum = 0;
	unsigned int pointLightNum = 0;
	//render lights
	for (Object* worldObj : objects)
	{
		if (worldObj->GetType() == ObjectType::Light) {
			Light* light = dynamic_cast<Light*>(worldObj);
			if (light->GetLightType() == LightType::Sun)
				light->Render(program, lightProgram, sunLightNum++);
			if (light->GetLightType() == LightType::Point)
				light->Render(program, lightProgram, pointLightNum++);
		}
	}
	program->SetUniformUInt(sunLightNum, "nbOfSunLights");
	program->SetUniformUInt(pointLightNum, "nbOfPointLights");

	//render Objects
	for (Object* worldObj : objects)
	{
		if (worldObj->GetType() == ObjectType::Mesh) {
			dynamic_cast<Mesh*>(worldObj)->Render(program);
		}
	}
}


void Scene::RenderDepths(Shader* depthShader, float minDist, float maxDist)
{
	//for point lights
	for (Object* worldObj : objects)
	{
		if (worldObj->GetType() == ObjectType::Light) {
			Light* light = dynamic_cast<Light*>(worldObj);
			if (light->GetLightType() == LightType::Point)
			{
				//for each light we compute the depth and the normals of the object around it --> results are stored
				// in 2 Cubemap textures (one for depth, and one for normals) to be able to have the data in every direction
				PointLight* ptLight = dynamic_cast<PointLight*>(light);
				CubemapPass(depthShader, light, ptLight->GetCubemapBufferPtr(), minDist, maxDist);
			}
		}
	}
}


// for depthMap (sun/directionnal lights) UNUSED
void Scene::DepthPass(Shader* depthShader, Light* light, DepthMap& depthMap, float minDist, float maxDist)
{
	//bind texture
	glBindTexture(GL_TEXTURE_2D, depthMap.depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	glBindFramebuffer(GL_FRAMEBUFFER, depthMap.depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap.depthMapTexture, 0);
	// don't need color buffer
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glm::mat4 lightProjection, lightView;

	glm::vec3 lookAtPoint = { 0,0,0 };
	glm::vec3 lookFrom = { 0,0,0 };
	glm::vec3 lookUpDirection = { 0,1,0 };
	if (light->GetLightType() == LightType::Point)
	{
		lightProjection = glm::perspective(glm::radians(90.f), 1.f, minDist, maxDist);
		lookFrom = dynamic_cast<PointLight*>(light)->GetPosition();
	}
	else
	{
		lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, minDist, maxDist);
		lookFrom = -(maxDist - minDist) * dynamic_cast<SunLight*>(light)->GetDirectionVector();
	}

	float cosTheta = glm::dot(glm::normalize(lookFrom - lookAtPoint), lookUpDirection);
	if (cosTheta >= 0.999999) 
	{
		lookAtPoint = glm::vec3(sin(acos(0.999999)), 0, 0);	//so that camera Up and lookAt Direction aren't perfectly aligned
																// but that we keep cameraUp = z axis
	}
	lightView = glm::lookAt(lookFrom, lookAtPoint, lookUpDirection);

	depthShader->SetUniformMat4(lightProjection, "projectionMatrix", GL_FALSE);
	depthShader->SetUniformMat4(lightView, "viewMatrix", GL_FALSE);

	glEnable(GL_DEPTH_TEST);

	//Peter panning
	glCullFace(GL_FRONT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMap.depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	depthShader->Use();
	// use nullptr as lightProgram so the light sphere doesn't interfere with depth
	RenderPass(depthShader, nullptr, SHADOW_WIDTH, SHADOW_HEIGHT); 
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//reset cull face mode
	glCullFace(GL_BACK);
	depthShader->Unuse();
}


// for cubemap (point lights)
void Scene::CubemapPass(Shader* depthCubemapShader, Light* light, CubemapBuffer* cubemapBuffer, float minDist, float maxDist)
{
	if (light->GetLightType() != LightType::Point)
		return;

	PointLight* pointLight = dynamic_cast<PointLight*>(light);

	if (!cubemapBuffer->isInit())	// create the textures for the cubemap
	{
		// ---- binding depth texture and setting every of its faces ----
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapBuffer->DepthTexture());

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		for (unsigned int i = 0; i < 6; ++i)
		{
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT,
					SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		}

		// ----  binding color texture (to store Normal.xyz in the rgb channels) and setting every of its faces ----
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapBuffer->NormalDataTexture());

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		for (unsigned int i = 0; i < 6; ++i)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA8,
				SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		}
		// --- Binding the frame buffer and linking both textures to it ----
		glBindFramebuffer(GL_FRAMEBUFFER, cubemapBuffer->FBO());

		//depth data textures
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, cubemapBuffer->DepthTexture(), 0);
		//normal data textures
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, cubemapBuffer->NormalDataTexture(), 0);
		//need color buffer to write the colors (the normals)
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		glReadBuffer(GL_COLOR_ATTACHMENT0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		cubemapBuffer->SetInitialized();
	}


	// create the 6 projection * view Matrices in the 6 directions around the light
	vector<glm::mat4> facesViewProjMatrices = cubemapBuffer->FacesViewProjMatrices(minDist, maxDist, pointLight->GetPosition());
	// giving those matrices to the shaders
	for (size_t i=0; i<facesViewProjMatrices.size(); i++)
	{
		depthCubemapShader->SetUniformMat4(facesViewProjMatrices[i], ("faceViewProjMatrices["+to_string(i)+"]").c_str());
	}
	// giving the light position and the farPlane to make sure the depth output is between 0 and 1
	depthCubemapShader->SetUniformVec3(pointLight->GetPosition(), "lightPos");
	depthCubemapShader->SetUniformFloat(maxDist, "far_plane");

	glEnable(GL_DEPTH_TEST);

	//Peter panning
	glCullFace(GL_FRONT);
	glBindFramebuffer(GL_FRAMEBUFFER, cubemapBuffer->FBO());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	depthCubemapShader->Use();
	// use nullptr as lightProgram so the light sphere doesn't interfere with depth
	// --- DrawCalls for all the objects in the Scene (world)
	RenderPass(depthCubemapShader, nullptr, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//reset cull face mode
	glCullFace(GL_BACK);
	depthCubemapShader->Unuse();
}


void Scene::Render(Shader* mainProgram, Shader* depthProgram, Shader* lightProgram, Shader* skyboxProgram,
	int framebufferWidth, int framebufferHeight, bool edgesOnly, float minDepthDist, float maxDepthDist, unsigned int targetFrameBuffer)
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //for all the tools passes before actual object render
	RenderDepths(depthProgram, minDepthDist, maxDepthDist);

	auto pointLights = getPointLights();

	// we only use point lights for subsurface scattering (for now)
	int lightIndex = 0;
	for (auto ptLight : pointLights)
	{
		// sending the textures to the mainProgram
		// depth Texture in texture unit 2*i
		mainProgram->SetUniformCubemapTexture(ptLight->GetCubemapBufferPtr()->DepthTexture(), ("lightDepthCubemap[" + std::to_string(lightIndex) + "]").c_str(), lightIndex);
		// normals Texture in texture unit 2*i+1
		mainProgram->SetUniformCubemapTexture(ptLight->GetCubemapBufferPtr()->NormalDataTexture(), ("lightNormalDataCubemap[" + std::to_string(lightIndex) + "]").c_str(), 2 * lightIndex + 1);
		lightIndex++;
	}

	// give the far plane distance to be able to find the real distance from the [0,1] depth
	mainProgram->SetUniformFloat(maxDepthDist, "far_plane");
	mainProgram->SetUniformUInt(pointLights.size(), "lightCount");

	//set render target
	glBindFramebuffer(GL_FRAMEBUFFER, targetFrameBuffer);

	glPolygonMode(GL_FRONT_AND_BACK, edgesOnly ? GL_LINE : GL_FILL); //decide here if we render the edges only, or the full objects

	//Render the whole scene (with the lights this time) for the screen
	RenderPass(mainProgram, lightProgram, framebufferWidth, framebufferHeight);

	if(useSkybox)
		skybox->Render(skyboxProgram);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //reset to full object rendering (to draw the quad and post processing)

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}