#pragma once

#include <vector>
#include "Object.h"
#include "Mesh.h"
#include "Light.h"
#include "Shader.h"
#include "DepthMap.h"
#include "Skybox.h"


class Scene
{
private: 
	std::string name;
	std::vector<Object*> objects;
	std::vector<CubemapBuffer> lightCubemapBuffers;

	Skybox* skybox;

	bool useSkybox = false;

	int selectedObjectIndex;

	bool hasAnObjectWithName(string name);
	std::vector<PointLight*> getPointLights();


public: 
	Scene();
	Scene(std::string world_name);
	~Scene();

	inline std::string GetName() const { return name; }
	inline unsigned int GetNbOfObj() const { return objects.size(); }
	inline std::vector<Object*> GetObjects() { return objects; }
	inline Object* GetObject(int index) { return objects[index]; }
	inline int GetSelectedObjectIndex() const { return selectedObjectIndex; }
	inline Object* GetSelectedObject() { 
		if (selectedObjectIndex == -1)
		{
			return nullptr;
		}
		return objects[selectedObjectIndex];
	}

	std::vector<Light*> GetLights();
	std::vector<CubemapBuffer> GetLightDepthCubemaps() { return lightCubemapBuffers; }
	bool* GetUseSkyboxPtr() { return &useSkybox; }

	void SelectObj(int objIndex);
	void UnselectAll();
	int RayFirstObjectTouched(glm::vec3 rayStart, glm::vec3 rayDirection);

	void AddObject(Object* obj);
	bool DeleteSelectedObject();

	void SetSkybox(std::vector<string> faceImgPaths);

	void RenderPass(Shader* program, Shader* lightProgram, int framebufferWidth, int framebufferHeight);
	void RenderDepths(Shader* depthShader, float minDist, float maxDist);
	void DepthPass(Shader* depthShader, Light* light, DepthMap& depthMap, float minDist, float maxDist);
	void CubemapPass(Shader* depthCubemapShader, Light* light, CubemapBuffer* depthCubemap, float minDist, float maxDist);
	void Render(Shader* mainProgram, Shader* depthProgram, Shader* lightProgram, Shader* skyboxProgram,
		int framebufferWidth, int framebufferHeight, bool edgesOnly, float minDepthDist, float maxDepthDist, unsigned int targetFrameBuffer=0);
};

