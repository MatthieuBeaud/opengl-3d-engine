#include "Shader.h"

#include <iostream>
#include <fstream>

using namespace std;

//Constructor / Destructor
Shader::Shader()
{
    programID = 0;
}

Shader::~Shader()
{
	glDeleteProgram(this->programID);
}

void Shader::Init(const string& vertexFileName, const string&  fragmentFileName, const string& geometryFileName)
{
    GLuint vertexShader = 0;
    GLuint geometryShader = 0;
    GLuint fragmentShader = 0;

    vertexFileSource = vertexFileName;
    fragmentFileSource = fragmentFileName;
    geometryFileSource = geometryFileName;

    vertexShader = loadShader(GL_VERTEX_SHADER, vertexFileName);
    if (geometryFileName != "")
    {
        geometryShader = loadShader(GL_GEOMETRY_SHADER, geometryFileName);
    }
    fragmentShader = loadShader(GL_FRAGMENT_SHADER, fragmentFileName);

    this->linkProgram(vertexShader, fragmentShader, geometryShader);

    //End
    glDeleteShader(vertexShader);
    glDeleteShader(geometryShader);
    glDeleteShader(fragmentShader);
}

void Shader::Reload()
{
    Init(vertexFileSource, fragmentFileSource, geometryFileSource);
}

//Public methods
void Shader::Use()
{
    glUseProgram(this->programID);
}

void Shader::Unuse() 
{
    glUseProgram(0);
}

void Shader::SetUniformUInt(GLuint value, const GLchar* name)
{
    this->Use();
    glUniform1ui(glGetUniformLocation(this->programID, name), value);
    this->Unuse();
}

void Shader::SetUniformTexture(GLuint value, const GLchar* name, const int textureSlot)
{
    this->Use();
    glUniform1i(glGetUniformLocation(this->programID, name), textureSlot);
    glActiveTexture(GL_TEXTURE0 + textureSlot);
    glBindTexture(GL_TEXTURE_2D, value);
    this->Unuse();
}

void Shader::SetUniformCubemapTexture(GLuint value, const GLchar* name, const int textureSlot)
{
    this->Use();
    //bind the texture in textureSlot X
    glActiveTexture(GL_TEXTURE0 + textureSlot);
    glBindTexture(GL_TEXTURE_CUBE_MAP, value);
    //say that the value for this name is in the texture slot X
    glUniform1i(glGetUniformLocation(this->programID, name), textureSlot);
    this->Unuse();
}

void Shader::SetUniformBool(GLboolean value, const GLchar* name)
{
    this->Use();
    glUniform1i(glGetUniformLocation(this->programID, name), value);
    this->Unuse();
}

void Shader::SetUniformInt(GLint value, const GLchar* name)
{
    this->Use();
    glUniform1i(glGetUniformLocation(this->programID, name), value);
    this->Unuse();
}

void Shader::SetUniformFloat(GLfloat value, const GLchar* name)
{
    this->Use();
    glUniform1f(glGetUniformLocation(this->programID, name), value);
    this->Unuse();
}

void Shader::SetUniformVec2(glm::vec2 value, const GLchar* name)
{
    this->Use();
    glUniform2fv(glGetUniformLocation(this->programID, name), 1, glm::value_ptr(value));
    this->Unuse();
}

void Shader::SetUniformVec3(glm::vec3 value, const GLchar* name)
{
    this->Use();
    glUniform3fv(glGetUniformLocation(this->programID, name), 1, glm::value_ptr(value));
    this->Unuse();
}

void Shader::SetUniformVec4(glm::vec4 value, const GLchar* name)
{
    this->Use();
    glUniform4fv(glGetUniformLocation(this->programID, name), 1, glm::value_ptr(value));
    this->Unuse();
}

void Shader::SetUniformMat3(glm::mat3 value, const GLchar* name, GLboolean transpose)
{
    this->Use();
    glUniformMatrix3fv(glGetUniformLocation(this->programID, name), 1, transpose, glm::value_ptr(value));
    this->Unuse();
}

void Shader::SetUniformMat4(glm::mat4 value, const GLchar* name, GLboolean transpose)
{
    this->Use();
    glUniformMatrix4fv(glGetUniformLocation(this->programID, name), 1, transpose, glm::value_ptr(value));
    this->Unuse();
}



//Private methods
string Shader::loadShaderSource(const string& fileName)
{
    string temp = "";
    string src = "";

    ifstream in_file;

    //Vertex shader;
    in_file.open(fileName);

    if (in_file.is_open())
    {
        while (getline(in_file, temp)) {
            src += temp + "\n";
        }
    }
    else {
        cout << "Error, shader couldn't open file: "<<fileName << endl;
    }
    in_file.close();

    return src;
}

GLuint Shader::loadShader(GLenum type, const string& fileName)
{
    char infoLog[512];
    GLint success;

    GLuint shader = glCreateShader(type);
    string stringSource = this->loadShaderSource(fileName);
    const GLchar* src = stringSource.c_str();
    glShaderSource(shader, 1, &src, NULL);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        cout << "error shader couldn't compile shader: "<< fileName << endl;
        cout << infoLog << endl;
    }

    return shader;
}

void Shader::linkProgram(GLuint vertexShader, GLuint fragmentShader, GLuint geometryShader)
{
    char infoLog[512];
    GLint success;

    this->programID = glCreateProgram();

    glAttachShader(this->programID, vertexShader);
    if (geometryShader)
    {
        glAttachShader(this->programID, geometryShader);
    }
    glAttachShader(this->programID, fragmentShader);


    glLinkProgram(this->programID);

    glGetProgramiv(this->programID, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(this->programID, 512, NULL, infoLog);
        cout << "error shader couldn't link program" << endl;
        cout << infoLog << endl;
    }

    glUseProgram(0);
}