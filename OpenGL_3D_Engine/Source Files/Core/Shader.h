#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>
#include <GLM/glm.hpp>
#include <GLM/gtc\type_ptr.hpp>

using namespace std;

class Shader
{
private:
	GLuint programID;
	string vertexFileSource;
	string fragmentFileSource;
	string geometryFileSource;

	string loadShaderSource(const string& fileName);
	GLuint loadShader(GLenum type, const string& fileName);
	void linkProgram(GLuint vertexShader, GLuint fragmentShader, GLuint geometryShader);

public:
	Shader();
	~Shader();
	void Init(const string& vertexFile, const string& fragmentFile, const string& geometryFile="");
	void Reload();

	inline GLuint GetID() const { return this->programID; }

	void Use();
	void Unuse();
	void SetUniformUInt(GLuint value, const GLchar* name);
	void SetUniformTexture(GLuint value, const GLchar* name, const int textureSlot);
	void SetUniformCubemapTexture(GLuint value, const GLchar* name, const int textureSlot);
	void SetUniformBool(GLboolean value, const GLchar* name);
	void SetUniformInt(GLint value, const GLchar* name);
	void SetUniformFloat(GLfloat value, const GLchar* name);
	void SetUniformVec2(glm::vec2 value, const GLchar* name);
	void SetUniformVec3(glm::vec3 value, const GLchar* name);
	void SetUniformVec4(glm::vec4 value, const GLchar* name);
	void SetUniformMat3(glm::mat3 value, const GLchar* name, GLboolean transpose = GL_FALSE);
	void SetUniformMat4(glm::mat4 value, const GLchar* name, GLboolean transpose = GL_FALSE);

};

