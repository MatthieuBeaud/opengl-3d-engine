#include "Skybox.h"
#include <STB/stb_image.h>


Skybox::Skybox()
{
    glGenTextures(1, &skyboxCubemapTexture);
    cube = nullptr;
}

Skybox::~Skybox()
{
    glDeleteTextures(1, &skyboxCubemapTexture);
    delete cube;
}

void Skybox::Init(std::vector<string> faceImgPaths)
{
    cube = new Mesh(Cube(), "skybox");

    LoadImages(faceImgPaths);
}

void Skybox::LoadImages(std::vector<string> faceImgPaths)
{
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxCubemapTexture);

    int width, height, nrChannels;
    for (unsigned int i = 0; i < faceImgPaths.size(); i++)
    {
        unsigned char* data = stbi_load(faceImgPaths[i].c_str(), &width, &height, &nrChannels, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
            );
            stbi_image_free(data);
        }
        else
        {
            std::cout << "Cubemap tex failed to load at path: " << faceImgPaths[i] << std::endl;
            stbi_image_free(data);
        }
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

void Skybox::Render(Shader* program)
{
    glDepthFunc(GL_LEQUAL);
    program->SetUniformCubemapTexture(skyboxCubemapTexture, "skybox", 2); //weird, when using textureslot 0, it shows
    //the shadow map of light n�0, texture slot 1 shows the normal pass from the light 1, but texture slot 2 works fine
    //even when initilizing the scene with 2 point lights ...
    cube->Render(program);
    glDepthFunc(GL_LESS);
}