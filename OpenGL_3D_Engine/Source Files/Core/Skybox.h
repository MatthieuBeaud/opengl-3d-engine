#pragma once

#include "Mesh.h"

class Skybox
{
private:
	Mesh* cube;
	unsigned int skyboxCubemapTexture;

	std::vector<string> facesImagesNames;

public: 
	Skybox();
	~Skybox();

	void Init(std::vector<string> faceImgPaths);

	void LoadImages(std::vector<string> faceImgPaths);

	void Render(Shader* program);

};

