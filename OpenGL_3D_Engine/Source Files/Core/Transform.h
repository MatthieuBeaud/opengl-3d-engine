#pragma once
#include <GLM/glm.hpp>

#define X_AXIS glm::vec3(1.f,0.f, 0.f)
#define Y_AXIS glm::vec3(0.f, 1.f, 0.f)
#define Z_AXIS glm::vec3(0.f, 0.f, 1.f)

struct Transform
{
	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 scale;

	Transform()
	{
		position = glm::vec3(0.f);
		rotation = glm::vec3(0.f);
		scale = glm::vec3(1.f);
	}
};