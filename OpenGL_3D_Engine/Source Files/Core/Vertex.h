#pragma once
#include <GLM/glm.hpp>

class Vertex
{
public:
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texCoord;

	Vertex(glm::vec3 pos, glm::vec3 n, glm::vec2 tex):
		position(pos), normal(n), texCoord(tex) {}

	Vertex(glm::vec3 pos, glm::vec3 n) :
		position(pos), normal(n), texCoord(glm::vec2(0.f)) {}

	Vertex(glm::vec3 pos) :
		position(pos), normal(glm::vec3(0.f)), texCoord(glm::vec2(0.f)) {}
};