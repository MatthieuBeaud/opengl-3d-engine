#pragma once
#include <GLM/glm.hpp>
#include <vector>
#include "IndexBuffer.h"
#include "Shader.h"

class Vertex2d
{
public:
	glm::vec2 position;
	glm::vec2 texCoord;

	Vertex2d(glm::vec2 pos, glm::vec2 tex) :
		position(pos), texCoord(tex) {}

};

class Quad2d
{

public:
	std::vector<Vertex2d> vertices;
	std::vector<GLuint> indices;

	VertexArray* va;
	VertexBuffer* vb;
	IndexBuffer* ib;

	Quad2d() //creates a quad (plane) width 1, height 1, centerPosition (0,0,0), normal (0,1,0)
	{
		Vertex2d quadVertices[] =
		{
			//Position							        //TexCoords        
			Vertex2d(glm::vec2(-1.f, 1.f),			glm::vec2(0.f, 1.f)),
			Vertex2d(glm::vec2(-1.f,-1.f),			glm::vec2(0.f, 0.f)),
			Vertex2d(glm::vec2(1.f,-1.f),			glm::vec2(1.f, 0.f)),
			Vertex2d(glm::vec2(1.f, 1.f),			glm::vec2(1.f, 1.f))
		};
		vertices = std::vector<Vertex2d>(quadVertices, quadVertices + 4);

		GLuint quadIndices[] =
		{
			0, 1, 2,
			0, 2, 3
		};

		indices = std::vector<GLuint>(quadIndices, quadIndices + 6);

		va = new VertexArray();
		vb = new VertexBuffer(vertices.data(), vertices.size() * sizeof(Vertex));
		ib = new IndexBuffer(indices.data(), indices.size());
		va->AddBuffer2d(*vb);
		va->Unbind();
	}

	~Quad2d() {}

	void Render(Shader* program, unsigned int textureToDisplay)
	{
		program->SetUniformTexture(textureToDisplay, "screenTexture", 0);
		program->Use();
		va->Bind();
		glDisable(GL_DEPTH_TEST);
		glBindTexture(GL_TEXTURE_2D, textureToDisplay);
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
		glBindTexture(GL_TEXTURE_2D, 0);
		va->Unbind();
		glEnable(GL_DEPTH_TEST);
	}
};