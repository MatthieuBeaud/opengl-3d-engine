#include "VertexArray.h"
#include "Vertex.h"
#include "Vertex2d.h"



VertexArray::VertexArray()
{
	glCreateVertexArrays(1, &this->id);
	glBindVertexArray(this->id);
}

VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &this->id);
}

void VertexArray::AddBuffer(const VertexBuffer& vb)
{
	Bind();
    vb.Bind();

    //attrib position of each vertex
    //in our case (see vertex_core.glsl) attribLocation of vertex_position is 0
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
    glEnableVertexAttribArray(0);

    //attrib normal of each vertex
    //in our case (see vertex_core.glsl) attribLocation of vertex_position is 1
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
    glEnableVertexAttribArray(1);

    //in our case (see vertex_core.glsl) attribLocation of vertex_color is 2
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoord));
    glEnableVertexAttribArray(2);
}

void VertexArray::AddBuffer2d(const VertexBuffer& vb)
{
    Bind();
    vb.Bind();

    //attrib position of each vertex
    //in our case (see vertex_postprocess.glsl) attribLocation of vertex_position is 0
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2d), (GLvoid*)offsetof(Vertex2d, position));
    glEnableVertexAttribArray(0);

    //in our case (see vertex_postprocess.glsl) attribLocation of vertex_color is 1
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2d), (GLvoid*)offsetof(Vertex2d, texCoord));
    glEnableVertexAttribArray(1);
}

void VertexArray::Bind()
{
	glBindVertexArray(this->id);
}

void VertexArray::Unbind()
{
	glBindVertexArray(0);
}
