#pragma once

#include"VertexBuffer.h"

class VertexArray 
{
private:
	GLuint id;

public:
	VertexArray();
	~VertexArray();

	void AddBuffer(const VertexBuffer& vertexBuffer);
	void AddBuffer2d(const VertexBuffer& vertexBuffer);
	void Bind();
	void Unbind();

	GLuint ID() { return id; }

};
