#include "VertexBuffer.h"
#include <iostream>


VertexBuffer::VertexBuffer()
{
	glGenBuffers(1, &id); //generates 1 buffer with the id gl_bufferID
}

VertexBuffer::VertexBuffer(const void* vertices, const unsigned int size)
{
	glGenBuffers(1, &id); //generates 1 buffer with the id gl_bufferID
	Bind();
	glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW); //sets the data with the vertices
														//use GL_DYNAMIC_DRAW if indices change often
}

VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers(1, &id);
}


void VertexBuffer::SetVertices(const void* vertices, const unsigned int size)
{
	Bind();
	glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
}

void VertexBuffer::Bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, id);
}

void VertexBuffer::Unbind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);

}