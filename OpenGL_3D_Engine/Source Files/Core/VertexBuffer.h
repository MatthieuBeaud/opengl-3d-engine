#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>


class VertexBuffer
{
private: 
	GLuint id;

public:
	VertexBuffer();
	VertexBuffer(const void* vertices, const unsigned int size);
	~VertexBuffer();

	void SetVertices(const void* vertices, const unsigned int size);
	void Bind() const;
	void Unbind() const;

	GLuint ID() { return id; }
};



