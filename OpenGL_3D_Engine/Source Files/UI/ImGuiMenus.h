#pragma once

#include <IMGUI/imgui.h>
#include <IMGUI/imgui_stdlib.h>
#include <IMGUI/imgui_impl_glfw.h>
#include <IMGUI/imgui_impl_opengl3.h>
#include "../Core/Color.h"
#include "../Core/Scene.h"
#include "../Core/Primitive.h"
#include "../Core/Axis.h"
#include "../Core/Grid.h"
#include "../Utils/FileDialogs.h"

static bool repositionWindow = true;
static bool showHelpWindow = false;
static string tempName = "";
static string objFilePath = "";
static int nbSegmentSphere = 16;
static int nbRingSphere = 8;

class ImGuiMenus
{
public:
	static void Init(GLFWwindow* window)
	{
		ImGui::CreateContext();
		ImGui::StyleColorsDark();
		ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_DockingEnable;
		ImGui_ImplGlfw_InitForOpenGL(window, true);
		ImGui_ImplOpenGL3_Init("#version 130");
	}

	static void NewFrame()
	{
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
	}

	static void Render()
	{
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	}

	static void ShutDown()
	{
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();
	}

	inline static bool WantCaptureMouse()
	{
		return ImGui::GetIO().WantCaptureMouse;
	}

	static void MainParameterWindow(Color* backgroundColor, Color* ambientLight, float* fov_ptr, Grid* grid, Axis* axis, bool& showPickerRay, int* colorsPerChannel)
	{
		
		ImGui::Begin("Render Parameters");    
		ImGui::ColorEdit3("ambient light", (float*)ambientLight);
		ImGui::ColorEdit3("background color", (float*)backgroundColor); // Edit 3 floats representing a color
		ImGui::NewLine();
		ImGui::DragFloat("camera FoV", fov_ptr, 1.f, 20.f, 160.f);
		ImGui::NewLine();
		ImGui::ColorEdit3("grid color", (float*)grid->GetColorPtr());	//if we change the color, we update the vertices colors of the grid
		if (ImGui::Button(grid->IsVisible() ? "Hide grid" : "Show grid")) 
		{
			grid->IsVisible() ? grid->Hide() : grid->Show();
		}
		if (ImGui::Button(axis->IsVisible() ? "Hide axis" : "Show axis"))
		{
			axis->IsVisible() ? axis->Hide() : axis->Show();
		}
		if (ImGui::Button(showPickerRay ? "Hide mouse picker ray" : "Show mouse picker ray"))
		{
			showPickerRay ? ((showPickerRay)=false) : ((showPickerRay)=true);
		}
		ImGui::NewLine();
		ImGui::TextWrapped("Application IO average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::NewLine();

		if (ImGui::Button(showHelpWindow?"Hide help":"Show help")) {
			showHelpWindow = !showHelpWindow;
		}
		ImGui::NewLine();
		ImGui::TextWrapped("Post Processing");
		ImGui::DragInt("Colors per channel", colorsPerChannel, 1, 1, 255);

		ImGui::End();
	}

	static void HelpsWindow() 
	{
		ImGui::Begin("Help", &showHelpWindow);
		ImGui::TextWrapped("Press T to switch camera type");
		ImGui::NewLine();
		ImGui::TextWrapped("Press E to switch between Edge view and Faces view");
		ImGui::NewLine();
		ImGui::TextWrapped("Press N to switch between faces normals and point normals");
		ImGui::NewLine();
		ImGui::TextWrapped("Press D to switch between Debug and Normal Modes");
		ImGui::NewLine();
		ImGui::TextWrapped("Press DEL or X to delete the currently selected object");
		ImGui::NewLine();
		ImGui::TextWrapped("Press R to reload the shaders");
		ImGui::NewLine();
		ImGui::TextWrapped("Use the left mouse button to select objects");
		ImGui::NewLine();
		ImGui::TextWrapped("Use the middle mouse button or ALT + left mouse button to rotate around");
		ImGui::NewLine();
		ImGui::TextWrapped("Use the scroolwheel to zoom in and out");
		ImGui::End();
	}

	static void WorldWindow(Scene& scene)
	{
		ImGui::Begin("World");

		ImGui::NewLine();
		ImGui::TextWrapped("World Settings");
		ImGui::Checkbox("Use Skybox", scene.GetUseSkyboxPtr());

		ImGui::NewLine();
		ImGui::TextWrapped("World Explorator");
		if (ImGui::TreeNodeEx(scene.GetName().c_str(), ImGuiTreeNodeFlags_DefaultOpen))
		{
			for (unsigned int i = 0; i< scene.GetNbOfObj(); i++)
			{
				if (ImGui::Selectable(scene.GetObject(i)->GetName().c_str(), scene.GetSelectedObjectIndex() == i))
				{
					scene.SelectObj(i);
				}
			}
			ImGui::TreePop();

			if (scene.GetSelectedObjectIndex() != -1) {
				ImGui::NewLine();
				//selected object Section
				worldObjectSection(scene.GetSelectedObject());
			}
		}
		else   //if tree closed, selectedObjIndex = -1
		{
			scene.SelectObj(-1);
		}

		ImGui::End();
	}

	static void NewObjectWindow(GLFWwindow* window, Scene& world)
	{
		ImGui::Begin("World Objects");
		if (ImGui::Button("Add New"))
		{
			ImGui::OpenPopup("Add New");
		}
		if(ImGui::BeginPopup("Add New"))
		{
			ImGui::Text("Meshes: ");
			if (ImGui::Button("Plane")) 
			{
				world.AddObject(new Mesh(Quad(), "plane"));
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::Button("Cube")) 
			{
				world.AddObject(new Mesh(Cube(), "cube"));
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::Button("Sphere"))
			{
				ImGui::OpenPopup("Create Sphere");
			}
			if (ImGui::BeginPopup("Create Sphere"))
			{
				ImGui::DragInt("Segments", &nbSegmentSphere, 1, 3, 128);
				ImGui::DragInt("Rings", &nbRingSphere, 1, 1, 128);
				if (ImGui::Button("Create"))
				{
					world.AddObject(new Mesh(Sphere(nbSegmentSphere, nbRingSphere), "sphere"));
					ImGui::CloseCurrentPopup();
				}
				ImGui::EndPopup();
			}

			ImGui::Text("Lights :");
			if (ImGui::Button("Sun"))
			{
				world.AddObject(new SunLight("SunLight", glm::vec3(1.f), 90 , 0.f, 10));
				ImGui::CloseCurrentPopup();
			}
			if (ImGui::Button("Point"))
			{
				world.AddObject(new PointLight("PointLight", glm::vec3(1.f), glm::vec3(0.f, 0.f, 5.f), 10));
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}
		if (ImGui::Button("Load obj from file"))
		{
			objFilePath = FileDialogs::OpenFile(window);
			if (!objFilePath.empty())
			{
				auto startPos = objFilePath.find_last_of('\\') + 1;
				auto endPos = objFilePath.find_last_of('.');
				world.AddObject(new Mesh(objFilePath.c_str(), objFilePath.substr(startPos, endPos-startPos)));
			}
		}
		ImGui::End();
	}

	static void TimelineWindow(AnimationSystem& animSystem, Scene& scene)
	{
		ImGui::Begin("Timeline");
		if (ImGui::DragInt("Frame Count", &animSystem.endFrame, 1, 1, MAX_FRAME))
		{
			if (animSystem.currentFrame > animSystem.endFrame)
				animSystem.currentFrame = animSystem.endFrame;
		}
		ImGui::TextWrapped("Timeline");
		if (ImGui::SliderInt("", &animSystem.currentFrame, animSystem.startFrame, animSystem.endFrame))
		{
			if(animSystem.animateOnFrameChange)
				animSystem.UpdateObjects();
		}
		ImGui::Checkbox("Animate", &animSystem.animateOnFrameChange);

		if (scene.GetSelectedObject() != nullptr)
		{
			auto obj = scene.GetSelectedObject();
			if(ImGui::Button("Add Keyframe"))
			{
				if (obj->GetType() == ObjectType::Mesh)
				{
					auto mesh = static_cast<Mesh*>(obj);
					animSystem.AddKeyframe(obj, animSystem.currentFrame, *mesh->GetTransformPtr());
				}
			}
			if (ImGui::Button("Remove Keyframe"))
			{
				animSystem.DeleteKeyframe(obj, animSystem.currentFrame);
			}
			ImGui::TextWrapped("KeyFrames at:");
			auto framesWithKeys = animSystem.GetObjKeyframes(obj);
			for (int frame : framesWithKeys)
			{
				if (ImGui::Button(std::to_string(frame).c_str()))
				{
					animSystem.currentFrame = frame;
					animSystem.UpdateObjects();
				}
			}
		}
		ImGui::End();
	}
private:

	static void worldObjectSection(Object* selectedObject)
	{
		if (selectedObject->GetType() == ObjectType::Mesh)
		{
			ImGui::TextWrapped("Mesh");
			Mesh* meshObject = dynamic_cast<Mesh*>(selectedObject);
			ImGui::DragFloat3("pos", (float*)(meshObject->GetPositionPtr()), 0.1f);
			ImGui::DragFloat3("rot", (float*)(meshObject->GetRotationPtr()), 1.f);
			ImGui::DragFloat3("size", (float*)(meshObject->GetScalePtr()), 0.1f, 0.f, 1000.f);
			ImGui::ColorEdit4("color", (float*)meshObject->GetColorPtr());
			ImGui::DragInt("shininess", (int*)meshObject->GetShininessPtr(), 1, 1, 128);
			ImGui::ColorEdit4("subsurface color", (float*)meshObject->GetSubsurfaceColorPtr());
			ImGui::DragFloat("density", meshObject->GetDensityPtr(), 0.1f, 0.1f, 10.f);

		}
		if (selectedObject->GetType() == ObjectType::Light)
		{
			Light* lightObject = dynamic_cast<Light*>(selectedObject);
			if (lightObject->GetLightType() == LightType::Sun)
			{
				SunLight* sunLight = dynamic_cast<SunLight*>(lightObject);
				ImGui::TextWrapped("SunLight");
				ImGui::TextWrapped("light angles:");
				ImGui::DragFloat("vertical", sunLight->GetThetaPtr(), 1.);
				ImGui::DragFloat("horizontal", sunLight->GetPhiPtr(), 1.);
				glm::vec3 dir = sunLight->GetDirectionVector();
				std::string direction = "direction: (" + std::to_string(dir.x) + ", " + std::to_string(dir.y) + ", " + std::to_string(dir.z) + ")";
				ImGui::TextWrapped(direction.c_str());
				ImGui::ColorEdit3("light color", (float*)sunLight->GetColorPtr());
			}
			if (lightObject->GetLightType() == LightType::Point)
			{
				PointLight* pointLight = dynamic_cast<PointLight*>(lightObject);
				ImGui::TextWrapped("PointLight");
				ImGui::DragFloat3("position", (float*)pointLight->GetPositionPtr(), 0.1f);
				ImGui::ColorEdit3("light color", (float*)pointLight->GetColorPtr());
			}
		}
		if (ImGui::InputTextWithHint("name", selectedObject->GetName().c_str(), &tempName, ImGuiInputTextFlags_EnterReturnsTrue))
		{
			selectedObject->SetName(tempName);
			tempName = "";
		}
	}
};

