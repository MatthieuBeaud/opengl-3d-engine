#pragma once

#include <string> 
#include "../Application.h"

class FileDialogs
{
public:
	static std::string OpenFile(GLFWwindow* window);
};
