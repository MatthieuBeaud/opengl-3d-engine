#pragma once

#include "GLM/glm.hpp"
#include "GLM/gtx/norm.hpp"
#include <iostream>

using namespace std;

class MathsHelper 
{
public:

	//returns the area of a triangle made by points a, b and c
	static float AreaOfTriangle(glm::vec3 a, glm::vec3 b, glm::vec3 c)
	{
		glm::vec3 ab = b - a;
		glm::vec3 ac = c - a;
		float theta = acos(glm::dot(ab, ac) / (glm::l2Norm(ab) * glm::l2Norm(ac)));
		float area =  glm::l2Norm(ab) * glm::l2Norm(ac) * sin(theta) /2;
		return area;
	}


	//check if m is in triangle made by a, b and c
	static bool PointInTriangle(glm::vec3 m, glm::vec3 a, glm::vec3 b, glm::vec3 c)
	{
		float totalArea = AreaOfTriangle(a, b, c);
		float areaSum = AreaOfTriangle(m, b, c) + AreaOfTriangle(a, m, c) + AreaOfTriangle(a, b, m);
		return abs(totalArea- areaSum) <= (0.01 * totalArea); // equal with max 1% relative error
	}

	static float RoundUp(float value, int decimals)
	{
		return  (float)round(value* pow(10, decimals)) / (float)pow(10, decimals);
	}


	static glm::vec3 NormalizedNormalOfTriangle(glm::vec3 a, glm::vec3 b, glm::vec3 c)
	{
		return glm::normalize(glm::cross(b - a, c - b));
	}
};




