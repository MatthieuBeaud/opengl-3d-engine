#include "application.h"


//main
int main(void)
{
    Application* app = new Application();

    if (!app->Init())  return -1;
    
    // Running the Initialized App
    app->Run();

    return 0;
}