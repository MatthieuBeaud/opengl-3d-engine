# OpenGL 3D Engine (to be renamed)

A 3D Engine made using OpenGL

## External Librairies

* GLEW (OpenGL Extension Wrangler Library)
* GLFW  (OpenGL Graphic Library Framework)

## Includes
These are used in the project, in "Source Files/vendor"
* GLM
* Dear ImGui (Docking Branch)


## Basic Viewer
You can load base meshes, add lights, or load from an .obj file, and play around with the scene settings (world background, grid, axis, camera field of view).
You can also switch between perspective and orthogonal view.
![Image 3](img/image3.jpg)
![Image 1](img/image1.jpg)
With mouse picking, you can select an object (or select it in the world objects panel). You can view and modify it's attributes (depending on its type). 
![Image 2](img/image2.jpg)
As an experiment I tried storing only the vertices, and computing the normal of each triangle in the shaders.
The implementation lighting (ambient, diffuse + specular (=Phong)), shows it works about right.
![Image 4](img/image4.jpg)

## Subsurface Scattering
You can also visualize an implementation sub surface scattering in real time.
It is possible to change the color that is absorbed at the surface of the mesh, or inside of the object, and see how it affects the subsurface scattering.
![Image 5](img/ssRed.png)
![Image 6](img/ssGreen.png)
![Image 7](img/ssBlue.png)
You can also see the impact of the density of the object on the effect. 
![Image 8: The impact of density (0.5 / 1 / 1.5 / 5 from left to right, top to bottom)](img/ssDensity.png)
